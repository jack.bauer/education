import React, { Component } from 'react';
import './App.css';
import Routes from "./Routes";
import { connect } from 'react-redux';
import { userSessionFetch } from './Actions/users';
import { withRouter } from 'react-router-dom'

class App extends Component {
  componentWillMount(){
      this.props.fetchSessionData()
  }

  render() {
    return (
      <div className="App">
        <div className="App-header crossfade">
            <figure />
            <figure />
            <figure />
            <figure />
            <figure />
        </div>
          <Routes />
      </div>
    );
  }
}

const mapStateToProps = (state) => {
    return {
        userSession: state.userSession,

    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        fetchSessionData: () => dispatch(userSessionFetch())
    };
};

export default withRouter(connect(mapStateToProps, mapDispatchToProps) (App));
