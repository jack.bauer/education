import React, { Component } from 'react';
import UserTable from "../Components/UserTable";
import NavigationPanel from '../Components/NavigationPanel';
import { connect } from 'react-redux';
import { usersFetchData } from '../Actions/users';
import WarningMessage from '../Components/WarningMessage'
import Loader from '../Components/Loader'
import AuthorizedComponent from '../Authorization/AuthorizedComponent'

class UsersList extends AuthorizedComponent {
    constructor(props) {
        super(props);

        this.roles = ['Administrator']
    }

    componentDidMount(){
        this.props.fetchData('http://localhost:8888/api/v1/users/getNotAdmin');
    }

    render() {
        if (this.props.hasErrored) {
            return (
                <div>
                    <NavigationPanel />
                    <div>
                        <WarningMessage message="There was an error loading users"/>
                    </div>
                </div>
            );
        }

        if (this.props.isLoading) {
            return (
                <div>
                    <NavigationPanel />
                    <div>
                        <Loader/>
                    </div>
                </div>
            );
        }

        return (
            <div>
                <NavigationPanel />
                <div>
                    <UserTable users={this.props.users}/>
                </div>
            </div>
        );
    }

}

const mapStateToProps = (state) => {
    return {
        users: state.users,
        hasErrored: state.userHasErrored,
        isLoading: state.userIsLoading,
        userSession: state.userSession
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        fetchData: (url) => dispatch(usersFetchData(url))
    };
};

export default connect(mapStateToProps, mapDispatchToProps) (UsersList);