import React, { Component } from 'react';
import { connect } from 'react-redux';
import { login } from '../Actions/users';
import SignIn from "../Components/SignIn";

class LoginContainer extends Component {
    render() {
        return (
            <div>
                <div>
                    <SignIn login={(obj) => this.props.login(obj)}/>
                </div>
            </div>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        userSession: state.userSession
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        login: (obj) => dispatch(login(obj))
    };
};

export default connect(mapStateToProps, mapDispatchToProps) (LoginContainer);
