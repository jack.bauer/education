import React, { Component } from 'react';
import Catalog from '../Components/Catalog';
import { categoryFetchData } from '../Actions/categories';
import { filteredItemFetch, filteredItemsCountFetchData, addCartItem } from '../Actions/items';
import { connect } from 'react-redux';

class ProductSearch extends Component {
    componentDidMount(){
        this.props.fetchCategories('http://localhost:8888/api/v1/categories/getAllCategories');
    }

    render() {
        return (
            <div>
                <Catalog onLoad={(url, obj) => this.props.fetchFilteredItemsCount(url, obj)} isLoading={this.props.filteredItemIsLoading}
                         categories={this.props.categories} filteredItems={this.props.filteredItems} filteredItemsCount={this.props.filteredItemsCount}
                         onSearch={(url, obj) => this.props.fetchFilteredItems(url, obj)} onAddToCart={(obj) => this.props.addItemToCart(obj)}/>
            </div>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        categories: state.categories,
        categoryHasErrored: state.categoryHasErrored,
        categoryIsLoading: state.categoryIsLoading,
        filteredItems: state.filteredItems,
        filterdItemHasErrored: state.filterdItemHasErrored,
        filteredItemIsLoading: state.filteredItemIsLoading,
        filteredItemCountHasErrored: state.filteredItemCountHasErrored,
        filteredItemCountIsLoading: state.filteredItemCountIsLoading,
        filteredItemsCount: state.filteredItemsCount

    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        fetchCategories: (url) => dispatch(categoryFetchData(url)),
        fetchFilteredItems: (url, obj) => dispatch(filteredItemFetch(url, obj)),
        fetchFilteredItemsCount: (url, obj) => dispatch(filteredItemsCountFetchData(url, obj)),
        addItemToCart: (obj) => dispatch(addCartItem(obj))
    };
};

export default connect(mapStateToProps, mapDispatchToProps) (ProductSearch);