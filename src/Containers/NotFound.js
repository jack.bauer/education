import React, { Component } from 'react';
import './NotFound.css';

class NotFound extends Component {
    render() {
        return (
            <div>
                <div className="not-found-body">
                    <h1 className="h1-error">404</h1>
                    <div >
                        <p className="p-not-found" >Unfortunately, the page you are looking for is unavailable<br/>
                            Please go back to <a href="/dashboard"> Dashboard</a>
                        </p>
                    </div>
                </div>
            </div>
        )

    }
}

export default NotFound;