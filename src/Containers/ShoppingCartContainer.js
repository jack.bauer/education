import React, { Component } from 'react';
import ShoppingCart from '../Components/ShoppingCart';
import { cartItemFetch, purchaseItems, deleteCartItem, addCartItem } from '../Actions/items';
import { connect } from 'react-redux';

class ShoppingCartContainer extends Component {
    componentDidMount(){
        this.props.getCartItems();
    }

    render() {
        return (
            <div>
                <ShoppingCart user={this.props.userSession} onCheckout={(obj) => this.props.purchaseItem(obj)} onDelete={(id) => this.props.deleteItemFromCart(id)}
                              purchaseStatus={this.props.purchaseStatus} cartItems={this.props.cartItems} onCartQtyChanged={(obj) => this.props.addItemToCart(obj)}/>
            </div>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        cartItems: state.cartItems,
        purchaseStatus: state.purchaseStatus,
        userSession: state.userSession,
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        getCartItems: () => dispatch(cartItemFetch()),
        purchaseItem: (obj) => dispatch(purchaseItems(obj)),
        deleteItemFromCart: (id) => dispatch(deleteCartItem(id)),
        addItemToCart: (obj) => dispatch(addCartItem(obj))
    };
};

export default connect(mapStateToProps, mapDispatchToProps) (ShoppingCartContainer);