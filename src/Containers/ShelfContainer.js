import React, { Component } from 'react';
import { categoryFetchData } from '../Actions/categories';
import { itemsFetchData, itemsCountFetchData } from '../Actions/items';
import { connect } from 'react-redux';
import CardShelf from "../Components/CardShelf";
import NavigationPanel from "../Components/NavigationPanel";
import AddCatalogItem from "../Components/AddCatalogItem";

class ShelfContainer extends Component {
    constructor(props) {
        super(props);

        if(this.props.location.state){
            if(this.props.location.state.disabled){
                this.state = {
                    itemsType: 'disabled',
                }
            } else if(this.props.location.state.zeroQty){
                this.state = {
                    itemsType: 'zeroQty',
                }
            } else if(this.props.location.state.general){
                this.state = {
                    itemsType: 'general',
                }
            }
        } else {
            this.state = {
                itemsType: 'general',
            }
        }
    }

    componentDidMount(){
        this.props.fetchCategories('http://localhost:8888/api/v1/categories/getAllCategories');
        if(this.state.itemsType==='general'){
            this.props.fetchItemsCount('http://localhost:8888/api/v1/items/getItemsCount/' + this.props.userSession.id);
        } else if(this.state.itemsType==='zeroQty'){
            this.props.fetchItemsCount('http://localhost:8888/api/v1/items/getAllItemsWithZeroQuantity/' + this.props.userSession.id);
        } else if(this.state.itemsType==='disabled'){
            this.props.fetchItemsCount('http://localhost:8888/api/v1/items/getCountOfAllDisabledItems/' + this.props.userSession.id);
        }
        // console.log(this.state.general)
    }

    render() {
        return (
            <div>
                <NavigationPanel/>
                <div>
                    <AddCatalogItem categories={this.props.categories} user={this.props.userSession}/>
                    <CardShelf userId={this.props.userSession.id} getItemsForPage={(url) => this.props.fetchItems(url)}
                               items={this.props.items} itemsCount={this.props.itemsCount} itemType={this.state.itemsType}/>
                </div>
            </div>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        userSession: state.userSession,
        categories: state.categories,
        categoryHasErrored: state.categoryHasErrored,
        categoryIsLoading: state.categoryIsLoading,
        items: state.items,
        itemHasErrored: state.itemHasErrored,
        itemIsLoading: state.itemIsLoading,
        itemsCount: state.itemsCount,
        itemCountHasErrored: state.itemCountHasErrored,
        itemCountIsLoading: state.itemCountIsLoading
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        fetchCategories: (url) => dispatch(categoryFetchData(url)),
        fetchItems: (url) => dispatch(itemsFetchData(url)),
        fetchItemsCount: (url) => dispatch(itemsCountFetchData(url)),
    };
};

export default connect(mapStateToProps, mapDispatchToProps) (ShelfContainer);