import React, { Component } from 'react';
import NavigationPanel from '../Components/NavigationPanel';
import Orders from '../Components/Orders'
import { connect } from 'react-redux';
import { getOrderItems } from '../Actions/orders';

class OrdersContainer extends Component {
    componentDidMount(){
        this.props.getOrders(this.props.userSession.id)
    }

    render() {
        return (
            <div>
                <NavigationPanel />
                <div>
                    <Orders orders={this.props.orders}/>
                </div>
            </div>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        orders: state.orders,
        userSession: state.userSession,
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        getOrders: (id) => dispatch(getOrderItems(id)),
    };
};

export default connect(mapStateToProps, mapDispatchToProps) (OrdersContainer);
