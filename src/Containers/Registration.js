import React, { Component } from 'react';
import { connect } from 'react-redux';
import { rolesFetchData } from '../Actions/roles';
import SignUp from '../Components/SignUp';

class Registration extends Component {
    componentDidMount() {
        this.props.fetchData('http://localhost:8888/api/v1/roles/getNewUserRoles');
    }

    render() {
        return (
            <SignUp roles={this.props.roles}/>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        roles: state.roles,
        rolesHasErrored: state.roleHasErrored,
        isLoading: state.roleIsLoading
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        fetchData: (url) => dispatch(rolesFetchData(url))
    };
};

export default connect(mapStateToProps, mapDispatchToProps) (Registration);