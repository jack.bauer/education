import React, {Component} from 'react';
import SweetAlert from 'react-bootstrap-sweetalert';
import './DeleteUser.css'

class DeleteUser extends Component {
    render() {
        return (
            <SweetAlert
                warning
                showCancel
                confirmBtnText="Yes, delete it!"
                confirmBtnBsStyle="danger"
                cancelBtnBsStyle="default"
                title="Are you sure you want to delete this user?"
                onConfirm={this.props.onConfirm}
                onCancel={this.props.onCancel}
            >
                Username: {this.props.user.username}
            </SweetAlert>
        )
    }
}

export default DeleteUser;