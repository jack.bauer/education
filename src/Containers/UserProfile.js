import React, { Component } from 'react';
import NavigationPanel from '../Components/NavigationPanel';
import Profile from '../Components/Profile';
import { profileFetchData } from '../Actions/profile';
import { connect } from 'react-redux';

class UserProfile extends Component {
    componentDidMount(){
        this.props.fetchUserProfile(this.props.userSession.id);
    }

    render() {
        return (
            <div>
                <NavigationPanel />
                <Profile userProfile={this.props.profile}/>
            </div>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        profile: state.profile,
        userProfileHasErrored: state.profileHasErrored,
        userProfileIsLoading: state.profileIsLoading,
        userSession: state.userSession
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        fetchUserProfile: (url) => dispatch(profileFetchData(url))
    };
};

export default connect(mapStateToProps, mapDispatchToProps) (UserProfile);
