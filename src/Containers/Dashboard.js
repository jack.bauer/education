import React, { Component } from 'react';
import NavigationPanel from '../Components/NavigationPanel';
import { withRouter } from 'react-router-dom'
import AuthorizedComponent from '../Authorization/AuthorizedComponent'
import { connect } from 'react-redux';
import './Dashboard.css'
import DashboardItem from '../Components/DashboardItem'
import { fetchUsersCount, fetchDisabledUsersCount, fetchCountOfUsersWithoutItems } from '../Actions/users';
import { fetchAllItemsCount, itemsCountFetchData, fetchCountWithDiscount, fetchCountWithZeroQty , fetchCountOfDisabledItemsById} from '../Actions/items';
import { fetchOrdersCount } from '../Actions/orders';

class Dashboard extends AuthorizedComponent {
    constructor(props) {
        super(props);

        this.roles = ['Buyer', 'Seller', 'Premium_Buyer', 'Administrator']
    }

    componentDidMount(){
        if(this.props.userSession.role === 'Administrator'){
            this.props.fetchUsersCount();
            this.props.fetchDisabledUsersCount();
            this.props.fetchAllItemsCount();
            this.props.fetchUsersWithoutItemsCount();
        } else if(this.props.userSession.role === 'Seller'){
            this.props.fetchItemsCount('http://localhost:8888/api/v1/items/getItemsCount/' + this.props.userSession.id);
            this.props.fetchAllItemsWithZeroQuantity(this.props.userSession.id);
            this.props.fetchAllDisabledItems(this.props.userSession.id);
        } else if(this.props.userSession.role === 'Buyer'){
            this.props.fetchAllItemsCount();
            this.props.fetchAllItemsWithDiscount();
            this.props.fetchAllOrdersCount(this.props.userSession.id);
        }
    }

    redirectToUsersPage = (e) => {
        e.preventDefault();
        this.props.history.push('users')
    }

    redirectToOrdersPage = (e) => {
        e.preventDefault();
        this.props.history.push('orders')
    }

    redirectToSearchPage = (e) => {
        e.preventDefault();
        this.props.history.push('search?page=0')
    }

    redirectToGoodsPage = (e) => {
        e.preventDefault();
        this.props.history.push({
            pathname: 'goods',
            state: { general: true }
        })
    }

    redirectToZeroGoodsPage = (e) => {
        e.preventDefault();
        this.props.history.push({
            pathname: 'goods',
            state: { zeroQty: true }
        })
    }

    redirectToDisableGoodsPage = (e) => {
        e.preventDefault();
        this.props.history.push({
            pathname: 'goods',
            state: { disabled: true }
        })
    }

    render() {
        return (
            <div>
                <NavigationPanel />
                <div id="page-wrapper">
                    <div className="row custom-row">
                        <div className="col-sm-12">
                            <h3 className="page-header">System Dashboard</h3>
                        </div>

                    </div>

                    <div className="row custom-row">
                        {(this.props.userSession.role === 'Administrator') ?
                            <div>
                                <DashboardItem redirectTo={true} redirectToPage={this.redirectToUsersPage} count={this.props.usersCount} colSize="col-sm-4"
                                               message="Total # of users in the system" className="panel panel-primary"/>
                                <DashboardItem redirectTo={true} redirectToPage={this.redirectToUsersPage} count={this.props.disabledUsersCount} colSize="col-sm-4"
                                               message="Total # of disabled users" className="panel panel-yellow"/>
                                <DashboardItem count={this.props.usersWithoutItemsCount} colSize="col-sm-4"
                                               message="Total # of sellers without items" className="panel panel-red"/>
                            </div>
                            : ((this.props.userSession.role === 'Buyer') || (this.props.userSession.role === 'Premium_Buyer')) ?
                                <div>
                                    <DashboardItem redirectTo={true} redirectToPage={this.redirectToSearchPage} count={this.props.allItemsCount} colSize="col-sm-4"
                                                   message="Total # of items in the system" className="panel panel-primary"/>
                                    <DashboardItem redirectTo={true} redirectToPage={this.redirectToSearchPage} count={this.props.countOfAllItemsWithDiscount} colSize="col-sm-4"
                                                   message="Total # of items with discount " className="panel panel-green"/>
                                    <DashboardItem redirectTo={true} redirectToPage={this.redirectToOrdersPage} count={this.props.allOrdersCount} colSize="col-sm-4"
                                                   message="Total # of closed orders" className="panel panel-yellow"/>
                                </div>
                                : (this.props.userSession.role === 'Seller') ?
                                    <div>
                                        <DashboardItem redirectTo={true} redirectToPage={this.redirectToGoodsPage} count={this.props.itemsCount} colSize="col-sm-4"
                                                       message="Total # of items" className="panel panel-primary"/>
                                        <DashboardItem redirectTo={true} redirectToPage={this.redirectToZeroGoodsPage} count={this.props.countOfAllItemsWithZeroQty} colSize="col-sm-4"
                                                       message="Total # of items with zero qty" className="panel panel-green"/>
                                        <DashboardItem redirectTo={true} redirectToPage={this.redirectToDisableGoodsPage} count={this.props.countOfAllDisabledItems} colSize="col-sm-4"
                                                       message="Total # of disabled items " className="panel panel-red"/>
                                    </div> : null
                        }
                    </div>
                </div>
            </div>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        userSession: state.userSession,
        usersCount: state.usersCount,
        disabledUsersCount: state.disabledUsersCount,
        usersWithoutItemsCount: state.usersWithoutItemsCount,
        allItemsCount: state.allItemsCount,
        countOfAllItemsWithDiscount: state.countOfAllItemsWithDiscount,
        countOfAllItemsWithZeroQty: state.countOfAllItemsWithZeroQty,
        countOfAllDisabledItems: state.countOfAllDisabledItems,
        itemsCount: state.itemsCount,
        allOrdersCount: state.allOrdersCount
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        fetchUsersCount: () => dispatch(fetchUsersCount()),
        fetchDisabledUsersCount: () => dispatch(fetchDisabledUsersCount()),
        fetchUsersWithoutItemsCount: () => dispatch(fetchCountOfUsersWithoutItems()),
        fetchAllItemsCount: () => dispatch(fetchAllItemsCount()),
        fetchAllItemsWithDiscount: () => dispatch(fetchCountWithDiscount()),
        fetchAllItemsWithZeroQuantity: (id) => dispatch(fetchCountWithZeroQty(id)),
        fetchAllDisabledItems: (id) => dispatch(fetchCountOfDisabledItemsById(id)),
        fetchItemsCount: (url) => dispatch(itemsCountFetchData(url)),
        fetchAllOrdersCount: (id) => dispatch(fetchOrdersCount(id)),
    };
};


export default connect(mapStateToProps, mapDispatchToProps) (Dashboard);
