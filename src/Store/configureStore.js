import { createStore, applyMiddleware } from 'redux';
import thunk from 'redux-thunk';
import rootReducer from '../reducers';
import { routerMiddleware } from 'react-router-redux';
import {BrowserRouter} from 'react-router-dom'

const configureStore = (initialState) => {
    return createStore(
        rootReducer,
        initialState,
        applyMiddleware(
            thunk
        )
    );

}

export default configureStore;