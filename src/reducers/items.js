import initialState from './initialState';
import * as types from '../Actions/actionTypes';

export function filterdItemHasErrored(state = false, action){
    if(action.type === types.FILTERED_ITEM_HAS_ERRORED){
        return action.filteredItemHasErrored;
    }
    return state;
};

export function filteredItemIsLoading(state = false, action){
    if(action.type === types.FILTERED_ITEM_IS_LOADING){
        return action.filteredItemIsLoading;
    }
    return state;
};

export function filteredItems(state = initialState.filteredItems, action){
    if(action.type === types.FILTERED_ITEM_FETCH_SUCCESS){
        return action.filteredItems;
    }
    return state;
};

export function itemHasErrored(state = false, action){
    if(action.type === types.ITEM_HAS_ERRORED){
        return action.itemHasErrored;
    }
    return state;
};

export function itemIsLoading(state = false, action){
    if(action.type === types.ITEM_IS_LOADING){
        return action.itemIsLoading;
    }
    return state;
};

export function items(state = initialState.items, action){
    if(action.type === types.ITEM_FETCH_SUCCESS){
        return action.items;
    }
    return state;
};

// items count for seller
export function itemCountHasErrored(state = false, action){
    if(action.type === types.ITEM_COUNT_HAS_ERRORED){
        return action.itemCountHasErrored;
    }
    return state;
};

export function itemCountIsLoading(state = false, action){
    if(action.type === types.ITEM_COUNT_IS_LOADING){
        return action.itemCountIsLoading;
    }
    return state;
};

export function itemsCount(state = initialState.itemsCount, action){
    if(action.type === types.ITEM_COUNT_FETCH_SUCCESS){
        return action.itemsCount;
    }
    return state;
};

//items count for buyer
export function filteredItemCountHasErrored(state = false, action){
    if(action.type === types.FILTERED_ITEM_COUNT_HAS_ERRORED){
        return action.filteredItemCountHasErrored;
    }
    return state;
};

export function filteredItemCountIsLoading(state = false, action){
    if(action.type === types.FILTERED_ITEM_COUNT_IS_LOADING){
        return action.filteredItemCountIsLoading;
    }
    return state;
};

export function filteredItemsCount(state = initialState.filteredItemsCount, action){
    if(action.type === types.FILTERED_ITEM_COUNT_FETCH_SUCCESS){
        return action.filteredItemsCount;
    }
    return state;
};

//cartItems
export function cartItems(state = initialState.cartItems, action){
    switch (action.type) {
        case types.GET_ALL_CART_ITEMS:
            return action.cartItems;
        case types.ADD_CART_ITEM:
            return [...state,action.cartItem];
        case types.CLEAR_CART:
            return initialState.cartItems;
        case types.DELETE_CART_ITEM:
            return [...state.filter((item) => item.id !== action.id)];
        default:
            return state;
    }

};

export function purchaseStatus(state = false, action){
    if(action.type === types.PURCHASE_SUCCESS){
        return Object.assign({}, state, {
            purchaseSuccess: true,
            purchaseInProgress: false,
            purchaseHasErrored: false,
        });
    }
    return state;
};

//items count
export function itemsCountFetchStatus(state = false, action){
    if(action.type === types.ALL_ITEMS_COUNT_FETCH_SUCCESS){
        return Object.assign({}, state, {
            allItemsFetchSuccess: true,
            allItemsFetchInProgress: false,
            allItemsFetchHasErrored: false,
        });
    }
    return state;
}

export function allItemsCount(state = initialState.allItemsCount, action){
    if(action.type === types.ALL_ITEMS_COUNT_FETCH){
        return action.allItemsCount;
    }
    return state;
};

//items count with discount
export function countOfItemsWithDiscountFetchStatus(state = false, action){
    if(action.type === types.FETCH_COUNT_OF_ALL_ITEMS_WITH_DISCOUNT_SUCCESS){
        return Object.assign({}, state, {
            itemsCountWithDiscountSuccess: true,
            itemsCountWithDiscountInProgress: false,
            itemsCountWithDiscountHasErrored: false,
        });
    }
    return state;
}

export function countOfAllItemsWithDiscount(state = initialState.countOfAllItemsWithDiscount, action){
    if(action.type === types.FETCH_COUNT_OF_ALL_ITEMS_WITH_DISCOUNT){
        return action.countOfAllItemsWithDiscount;
    }
    return state;
};

//items count with zero quantity
export function countOfItemsWithZeroQtyFetchStatus(state = false, action){
    if(action.type === types.FETCH_COUNT_OF_ITEMS_WITH_ZERO_QUANTITY_SUCCESS){
        return Object.assign({}, state, {
            itemsCountWithZeroQtySuccess: true,
            itemsCountWithZeroQtyInProgress: false,
            itemsCountWithZeroQtyHasErrored: false,
        });
    }
    return state;
}

export function countOfAllItemsWithZeroQty(state = initialState.countOfAllItemsWithZeroQty, action){
    if(action.type === types.FETCH_COUNT_OF_ITEMS_WITH_ZERO_QUANTITY){
        return action.countOfAllItemsWithZeroQty;
    }
    return state;
};

//disabled items count
export function countOfDisabledItemsFetchStatus(state = false, action){
    if(action.type === types.FETCH_COUNT_OF_DISABLED_ITEMS_SUCCESS){
        return Object.assign({}, state, {
            disabledItemsCountSuccess: true,
            disabledItemsCountInProgress: false,
            disabledItemsCountHasErrored: false,
        });
    }
    return state;
}

export function countOfAllDisabledItems(state = initialState.countOfDisabledItems, action){
    if(action.type === types.FETCH_COUNT_OF_DISABLED_ITEMS){
        return action.countOfDisabledItems;
    }
    return state;
};
