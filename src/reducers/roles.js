import initialState from './initialState';
import * as types from '../Actions/actionTypes';

// export default function roleReducer(state = initialState.roles, action) {
//     switch(action.type) {
//         case types.ROLE_FETCH_DATA_SUCCESS:
//             return action.roles;
//         default:
//             return state;
//     }
// };
//
// export function roleLoadingReducer(state = false, action) {
//     switch(action.type) {
//         case types.ROLE_IS_LOADING:
//             return action.isLoading;
//         case types.ROLE_HAS_ERRORED:
//             return action.hasErrored;
//         default:
//             return state;
//     }
// }

export function roleHasErrored(state = false, action){
    if(action.type === types.ROLE_HAS_ERRORED){
        return action.hasErrored;
    }
    return state;
};

export function roleIsLoading(state = false, action){
    if(action.type === types.ROLE_IS_LOADING){
        return action.isLoading;
    }
    return state;
};

export function roles(state = initialState.roles, action){
    if(action.type === types.ROLE_FETCH_DATA_SUCCESS){
        return action.roles;
    }
    return state;
};

// export function countHasErrored(state = false, action){
//     if(action.type === 'COUNT_HAS_ERRORED'){
//         return action.hasErrored;
//     }
//     return state;
// };
//
// export function countIsLoading(state = false, action){
//     if(action.type === 'COUNT_IS_LOADING'){
//         return action.isLoading;
//     }
//     return state;
// };
//
// export function count(state = initialState, action){
//     if(action.type === 'COUNT_FETCH_DATA_SUCCESS'){
//         return action.count;
//     }
//     return state;
// };
//
// export function topicsHasErrored(state = false, action){
//     if(action.type === 'TOPICS_HAS_ERRORED'){
//         return action.hasErrored;
//     }
//     return state;
// };
//
// export function topicsIsLoading(state = false, action){
//     if(action.type === 'TOPICS_IS_LOADING'){
//         return action.isLoading;
//     }
//     return state;
// };
//
// export function topicItems(state = initialState, action){
//     if(action.type === 'TOPICS_FETCH_DATA_SUCCESS'){
//         return action.topicItems;
//     }
//     return state;
// };