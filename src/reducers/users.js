import initialState from './initialState';
import * as types from '../Actions/actionTypes';

export function userHasErrored(state = false, action){
    if(action.type === 'USER_HAS_ERRORED'){
        return action.hasErrored;
    }
    return state;
};

export function userIsLoading(state = false, action){
    if(action.type === 'USER_IS_LOADING'){
        return action.isLoading;
    }
    return state;
};

export function users(state = initialState.users, action){
    if(action.type === 'USER_FETCH_DATA_SUCCESS'){
        return action.users;
    }
    return state;
};


export function newUser(state = initialState.newUser, action){
    if(action.type === 'ADD_USER_SUCCESS'){
        return [
            ...state,
            action.payload
        ];
    }
    return state;
};

//login
export function userSession(state = initialState.userSession, action){
    switch (action.type) {
        // case types.GET_ALL_CART_ITEMS:
        //     return action.cartItems;
        case types.LOGIN:
            return action.userSession;
        case types.FETCH_USER_SESSION:
            return action.userSession;
        case types.LOGOUT:
             return state;
        default:
            return state;
    }

};

export function loginStatus(state = false, action){
    if(action.type === types.LOGIN_SUCCESS){
        return Object.assign({}, state, {
            loginSuccess: true,
            loginInProgress: false,
            loginHasErrored: false,
        });
    }
    return state;
};

export function logoutStatus(state = false, action){
    if(action.type === types.LOGOUT_SUCCESS){
        return Object.assign({}, state, {
            logoutSuccess: true,
            logoutInProgress: false,
            logoutHasErrored: false,
        });
    }
    return state;
};

export function usersCountStatus(state = false, action){
    if(action.type === types.USERS_COUNT_FETCH_SUCCESS){
        return Object.assign({}, state, {
            usersFetchSuccess: true,
            usersFetchInProgress: false,
            usersFetchHasErrored: false,
        });
    }
    return state;
}

export function usersCount(state = initialState.usersCount, action){
    if(action.type === types.USERS_COUNT_FETCH){
        return action.usersCount;
    }
    return state;
};

//disabled users count
export function disabledUsersCountStatus(state = false, action){
    if(action.type === types.DISABLED_USERS_COUNT_FETCH_SUCCESS){
        return Object.assign({}, state, {
            disabledUsersFetchSuccess: true,
            disabledUsersFetchInProgress: false,
            disabledUsersFetchHasErrored: false,
        });
    }
    return state;
}

export function disabledUsersCount(state = initialState.disabledUsersCount, action){
    if(action.type === types.DISABLED_USERS_COUNT_FETCH){
        return action.disabledUsersCount;
    }
    return state;
};

//users without items count
export function usersWithoutItemsCountStatus(state = false, action){
    if(action.type === types.USERS_WITHOUT_ITEMS_COUNT_FETCH_SUCCESS){
        return Object.assign({}, state, {
            usersWithoutItemsFetchSuccess: true,
            usersWithoutItemsFetchInProgress: false,
            usersWithoutItemsFetchHasErrored: false,
        });
    }
    return state;
}

export function usersWithoutItemsCount(state = initialState.usersWithoutItemsCount, action){
    if(action.type === types.USERS_WITHOUT_ITEMS_COUNT_FETCH){
        return action.usersWithoutItemsCount;
    }
    return state;
};

// console.log(obj.filter(({ id }) => id !== 2));


// export function countHasErrored(state = false, action){
//     if(action.type === 'COUNT_HAS_ERRORED'){
//         return action.hasErrored;
//     }
//     return state;
// };
//
// export function countIsLoading(state = false, action){
//     if(action.type === 'COUNT_IS_LOADING'){
//         return action.isLoading;
//     }
//     return state;
// };
//
// export function count(state = initialState, action){
//     if(action.type === 'COUNT_FETCH_DATA_SUCCESS'){
//         return action.count;
//     }
//     return state;
// };
//
// export function topicsHasErrored(state = false, action){
//     if(action.type === 'TOPICS_HAS_ERRORED'){
//         return action.hasErrored;
//     }
//     return state;
// };
//
// export function topicsIsLoading(state = false, action){
//     if(action.type === 'TOPICS_IS_LOADING'){
//         return action.isLoading;
//     }
//     return state;
// };
//
// export function topicItems(state = initialState, action){
//     if(action.type === 'TOPICS_FETCH_DATA_SUCCESS'){
//         return action.topicItems;
//     }
//     return state;
// };