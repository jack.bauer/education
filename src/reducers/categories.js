import initialState from './initialState';
import * as types from '../Actions/actionTypes';

export function categoryHasErrored(state = false, action){
    if(action.type === types.CATEGORY_HAS_ERRORED){
        return action.categoryHasErrored;
    }
    return state;
};

export function categoryIsLoading(state = false, action){
    if(action.type === types.CATEGORY_IS_LOADING){
        return action.categoryIsLoading;
    }
    return state;
};

export function categories(state = initialState.categories, action){
    if(action.type === types.CATEGORY_FETCH_DATA_SUCCESS){
        return action.categories;
    }
    return state;
};