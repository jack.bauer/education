import initialState from './initialState';

export function profileHasErrored(state = false, action){
    if(action.type === 'PROFILE_HAS_ERRORED'){
        return action.profileHasErrored;
    }
    return state;
};

export function profileIsLoading(state = false, action){
    if(action.type === 'PROFILE_IS_LOADING'){
        return action.profileIsLoading;
    }
    return state;
};

export function profile(state = initialState.profile, action){
    if(action.type === 'PROFILE_FETCH_DATA_SUCCESS'){
        return action.profile;
    }
    return state;
};