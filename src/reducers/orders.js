import initialState from './initialState';
import * as types from '../Actions/actionTypes';

export function orderFetchSuccess(state = false, action){
    if(action.type === types.ORDER_FETCH_SUCCESS){
        return Object.assign({}, state, {
            orderFetchSuccess: true,
            orderIsLoading: false,
            orderHasHasErrored: false,
        });
    }
    return state;
};

export function orders(state = initialState.orders, action){
    if(action.type === types.ORDER_FETCH_SUCCESS){
        return action.orders;
    }
    return state;
};

//Orders reducer
export function allOrdersCountStatus(state = false, action){
    if(action.type === types.ORDERS_COUNT_FETCH_SUCCESS){
        return Object.assign({}, state, {
            allOrdersCountFetchSuccess: true,
            allOrdersCountFetchInProgress: false,
            allOrdersCountFetchHasErrored: false,
        });
    }
    return state;
}

export function allOrdersCount(state = initialState.allOrdersCount, action){
    if(action.type === types.ORDERS_COUNT_FETCH){
        return action.allOrdersCount;
    }
    return state;
};