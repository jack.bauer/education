import { combineReducers } from 'redux';
import { routerReducer } from 'react-router-redux';

import { userHasErrored, userIsLoading, users, newUser, loginStatus, userSession, usersCount, usersCountStatus,
    disabledUsersCountStatus, disabledUsersCount, usersWithoutItemsCount, usersWithoutItemsCountStatus, logoutStatus } from './users';
import { roleHasErrored, roleIsLoading, roles } from './roles';
import { profileHasErrored, profileIsLoading, profile } from './profile';
import { categoryHasErrored, categoryIsLoading, categories } from './categories';
import { orderFetchSuccess, orders, allOrdersCount, allOrdersCountStatus } from './orders';
import { filterdItemHasErrored, filteredItemIsLoading, filteredItems, itemHasErrored, itemIsLoading, items,
    itemCountHasErrored, itemCountIsLoading, itemsCount, filteredItemCountHasErrored, filteredItemCountIsLoading, filteredItemsCount, cartItems,
    purchaseStatus, allItemsCount, itemsCountFetchStatus, countOfAllItemsWithDiscount, countOfItemsWithDiscountFetchStatus,
    countOfAllItemsWithZeroQty, countOfItemsWithZeroQtyFetchStatus, countOfAllDisabledItems, countOfDisabledItemsFetchStatus} from './items';

const allReducers = combineReducers({
    router: routerReducer,
    userHasErrored,
    userIsLoading,
    users,
    newUser,
    loginStatus,
    logoutStatus,
    userSession,
    usersCount,
    usersCountStatus,
    disabledUsersCountStatus,
    disabledUsersCount,
    usersWithoutItemsCount,
    usersWithoutItemsCountStatus,
    roleHasErrored,
    roleIsLoading,
    roles,
    profileHasErrored,
    profileIsLoading,
    profile,
    categoryHasErrored,
    categoryIsLoading,
    categories,
    filterdItemHasErrored,
    filteredItemIsLoading,
    filteredItems,
    itemHasErrored,
    itemIsLoading,
    items,
    itemCountHasErrored,
    itemCountIsLoading,
    itemsCount,
    filteredItemCountHasErrored,
    filteredItemCountIsLoading,
    filteredItemsCount,
    cartItems,
    allItemsCount,
    itemsCountFetchStatus,
    countOfAllItemsWithDiscount,
    countOfItemsWithDiscountFetchStatus,
    countOfAllItemsWithZeroQty,
    countOfItemsWithZeroQtyFetchStatus,
    countOfAllDisabledItems,
    countOfDisabledItemsFetchStatus,
    purchaseStatus,
    orderFetchSuccess,
    orders,
    allOrdersCount,
    allOrdersCountStatus
});

const rootReducer = (state, action) => {
    return allReducers(state, action)
}

export default rootReducer;