import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import { Provider } from 'react-redux';
import registerServiceWorker from './registerServiceWorker';
import {BrowserRouter} from 'react-router-dom'
import configureStore from './Store/configureStore'
import Bootstrap from 'bootstrap/dist/css/bootstrap.css';
import { createStore, combineReducers, applyMiddleware } from 'redux'
import Root from "./Root";
import { ConnectedRouter, routerReducer, routerMiddleware, push } from 'react-router-redux'
import createHistory from 'history/createBrowserHistory'
import rootReducer from './reducers';
import thunk from 'redux-thunk';
import { userSessionFetch } from './Actions/users';
import { loadSessionState } from "./localStorage"

// const configureStore = (initialState) => {
//     return createStore(
//         rootReducer,
//         initialState,
//         applyMiddleware(
//             thunk
//         )
//     );
// }


const history = createHistory()
const middleware = routerMiddleware(history)

const store = createStore(
    rootReducer,
    // loadSessionState(),
    window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__(),
    applyMiddleware(
        thunk,
        middleware,
    )
)


// const store = configureStore(window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__());

ReactDOM.render(
    <Root store={store} history={history}/>,
    document.getElementById('root')
)
