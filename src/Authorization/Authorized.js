import React from 'react';
import { connect } from 'react-redux';

class Authorized extends React.PureComponent {
    render() {
        const currentRole = this.props.userSession.role;
        if (this.props.roles.includes(currentRole)) {
            return (this.props.children)
        } else {
            return (
                <span></span>
            )
        }
    }
}

const mapStateToProps = (state) => {
    return {
        userSession: state.userSession
    };
};


export default connect(mapStateToProps) (Authorized);