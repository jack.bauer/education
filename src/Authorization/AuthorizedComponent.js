import React, { Component } from 'react';
// import {roles} from "../reducers/roles";
import {withRouter} from 'react-router-dom';

class AuthorizedComponent extends Component {
    componentWillMount(){
        this.role = this.props.userSession.role;
        if(!this.roles.includes(this.role)){
            this.props.history.push("restricted")
        }
    }
}


export default AuthorizedComponent;