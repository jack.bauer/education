import React, { PureComponent } from 'react';
import './OrderItem.css';
import OrderItemsTable from "./OrderItemsTable";

class OrderItem extends PureComponent {
    constructor(props) {
        super(props);
    }

    render() {
        let index = '#'+ this.props.index
        return (
            <div className="panel panel-default">
                <div className="panel-heading " role="tab" id="headingTwo">
                    <h4 className="order-panel-title">
                        <a className="collapsed" data-toggle="collapse" data-parent="#accordion" href={index} aria-expanded="false" aria-controls={this.props.index}>
                            <div className="row custom-order-row">
                                <div className="col-sm-6">
                                    <span style={{fontSize: '14px'}}>#{this.props.item.orderId}</span>
                                    <span style={{paddingLeft: '10px', fontSize: '14px'}}>Order date: {this.props.item.date}</span>
                                </div>
                                <div className="col-sm-6" style={{textAlign: 'right', fontSize: '14px'}}>
                                    <span>Order Price: <b>{this.props.item.totalPrice}$</b></span>
                                </div>
                            </div>
                        </a>
                    </h4>
                </div>
                <div id={this.props.index} className="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
                    <div className="panel-body">
                        <OrderItemsTable items={this.props.item.items}/>
                    </div>
                </div>
            </div>
        )

    }
}

export default OrderItem;