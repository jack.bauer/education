import React, { Component } from 'react';
import '../Containers/Dashboard.css';

class DashboardItem extends Component {
    constructor(props) {
        super(props);

    }

    render() {
        return (
            <div className={this.props.colSize}>
                <div className={this.props.className}>
                    <div className="panel-heading">
                        <div className="row custom-row">
                            <div className="col-xs-3">
                                <i className="fa fa-comments fa-5x"></i>
                            </div>
                            <div className="col-xs-9 text-right">
                                <div className="huge">{this.props.count}</div>
                                <div className="message">{this.props.message}</div>
                            </div>
                        </div>
                    </div>
                    <a href="#">
                        <div className="panel-footer" style={{cursor: 'default'}}>
                            {this.props.redirectTo ?
                                <div  style={{cursor: 'pointer'}}>
                                    <div className="pull-left" onClick={this.props.redirectToPage}>View Details</div>
                                    <span className="pull-right"><i className="fa fa-arrow-circle-right"></i></span>
                                    <div className="clearfix"></div>
                            </div> : <div>
                                    <div className="pull-left"></div>
                                    <span className="pull-right" style={{paddingTop: '10px', paddingBottom:'10px'}}><i className="fa fa-arrow-circle-right"></i></span>
                                    <div className="clearfix"></div>
                                </div>
                            }
                        </div>
                    </a>
                </div>
            </div>
        )
    }
}

export default DashboardItem;