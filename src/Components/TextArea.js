import React, { Component } from 'react';
import './TextArea.css';

class TextArea extends Component{
  constructor(props) {
      super(props);
      this.state = {
          field: this.props.field,
      }
  }

  onChange = (event) => {
      let value = event.target.value;
      this.setState({
          field: value,
      });
      if (this.props.onTextChanged) {
          this.props.onTextChanged(value);
      }
  }

  render() {
      if(this.props.required){
          return (
              <textarea style={this.props.style} required className="desc-area" maxLength={this.props.max} rows={this.props.rowSize} cols={this.props.colSize} onChange={this.onChange}
                        placeholder={this.props.placeholderText} value={this.props.field} />
          )
      } else {
          return (
              <textarea style={this.props.style} className="desc-area" maxLength={this.props.max} rows={this.props.rowSize} cols={this.props.colSize} onChange={this.onChange}
                        placeholder={this.props.placeholder} value={this.props.field} />
          )
      }
  }
}

export default TextArea;