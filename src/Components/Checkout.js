import React, {Component} from 'react';
import './Checkout.css'
import { Modal, Button } from 'react-bootstrap';
import Input from '../Components/Input';
import ExpirationDateDropdown from "./ExpirationDateDropdown";
import {months, years} from '../dates';

class Checkout extends Component {
    constructor(props) {
        super(props);

        this.state = {
            cardNo: 'XXXX XXXX XXXX XXXX',
            cardHolder: 'IVAN IVANOV',
            active: this.props.active,
            cardType: null,
            cvc: 'XXX',
            expirationMonth: '01',
            expirationYear: '01',
            alert: null,
            success: false
        };
    }

    onCardTypeChange = (event) => {
        let value = event.target.id;
        this.setState({
            cardType: value,
        });

    }

    cardNoChanged = (text) => {
        if(text.startsWith('4')){
            this.setState({
                cardType: 'visa'
            })
        } else if(text.startsWith('51') || text.startsWith('52')) {
            this.setState({
                cardType: 'mastercard'
            })
        }

        this.setState({
            cardNo: text,
        })

    }

    cardHolderChanged = (text) => {
        this.setState({
            cardHolder: text,
        })
    }

    cvcChanged = (text) => {
        this.setState({
            cvc: text,
        })
    }

    onMonthChanged = (text) => {
        this.setState({
            expirationMonth: text,
        })
    }

    onYearChanged = (text) => {
        this.setState({
            expirationYear: text,
        })
    }

    purchase = () => {
        this.props.onPurchase({
            buyerId: 2,
            itemIds: this.props.itemsToPurchase
        })

         this.props.close()
    }

    render() {
        return (
            <div>
                <Modal className="checkout-modal-container "
                       show={this.props.show}
                       onHide={this.props.close}
                       animation={false}
                       bsSize="large">
                <form onSubmit={this.purchase}>
                    <Modal.Body>
                        <div id="checkout-box">
                            <div className="left-checkout">
                                <h1>Order checkout</h1>
                                <div className={this.state.cardType==="visa" ? 'cc-visa' : this.state.cardType==="mastercard" ? 'cc-master' : 'cc'}>
                                    <span className="chip"></span>
                                        {this.state.cardType==="visa" ? <div><span className="card-type visa-card"> </span>
                                            <span className="paywave"></span>
                                        </div> : this.state.cardType==="mastercard" ? <div><span className="card-type master-card"> </span>
                                                <span className="paypass"></span>
                                            </div> : <span className="card-type"> </span>
                                        }
                                    <span className="cc-number">{this.state.cardNo}</span>
                                    <span className="cc-date cc-card no"><p>{this.state.cardNo.substring(0,4)}</p></span>
                                    <span className="cc-date expiry"><p>valid <br/> thru</p>{this.state.expirationMonth}/{this.state.expirationYear}</span>
                                    <span className="cc-name">{this.state.cardHolder}</span>
                                </div>
                                <div className={this.state.cardType==="visa" ? 'cc-visa-back' : this.state.cardType==="mastercard" ? 'cc-master-back' : 'cc-back'}>
                                    <div className="service-call"><p>For customer service call +375 17 289 92 92</p></div>
                                    <div className="magnetic-back"></div>
                                    <div className="authorized"><p>AUTHORIZED SIGNATURE - NOT VALID UNLESS SIGNED</p></div>
                                    <div className="sign-back"><p>{this.state.cvc}</p></div>
                                </div>
                            </div>
                            <div className="right-in-checkout">
                                <input className="card" id="visa" type="button" name="card" value="" onClick={this.onCardTypeChange}/>
                                <input className="card" id="mastercard" type="button" name="card" value="" onClick={this.onCardTypeChange}/>
                                <label>Credit Card Number</label>
                                <Input className="checkout-input" required min="15" max="19" type="text" placeholder="XXXX XXXX XXXX XXXX"
                                       onTextChanged={this.cardNoChanged} restrictType="text"/>
                                <label>Card Holder</label>
                                <Input className="checkout-input card-holder" required min="5" max="26" type="text" placeholder="IVAN IVANOV"
                                       onTextChanged={this.cardHolderChanged} restrictType="number"/>
                                <label>Expiration Date</label>
                                <label id="cvc-label" >CVC/CVV</label>
                                <div className="row" style={{marginTop: 0}}>
                                    <div className="col-md-4">
                                        <ExpirationDateDropdown name="months" items={months} style={{width: '55px', paddingRight: 0}}
                                                                required max="3" type="text" placeholder="MM" onTextChanged={this.onMonthChanged}/>
                                    </div>
                                    <div className="col-md-4" style={{marginLeft: '-40px'}}>
                                        <ExpirationDateDropdown name="years" items={years} style={{width: '55px', paddingLeft: 0}}
                                                                max="3" type="text" placeholder="YY" onTextChanged={this.onYearChanged}/>
                                    </div>
                                    <div className="col-md-4">
                                        <Input className="checkout-input" style={{width: '60px'}} required min="3" max="3" type="text" placeholder="CVC/CVV"
                                               restrictType="text" onTextChanged={this.cvcChanged}/>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <p className="footer">
                            <span>
                                <Button style={{marginRight: '8px'}} className="btn btn-lg btn-default" onClick={this.props.close}>Cancel</Button>
                            </span>
                            <span>
                                <Button className="btn btn-lg btn-info" type="submit">Purchase</Button>
                            </span>
                        </p>
                    </Modal.Body>
                </form>
                </Modal>
                {this.state.alert}
            </div>
        )
    }
}

export default Checkout;