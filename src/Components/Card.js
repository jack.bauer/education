import React, {Component} from 'react';
import './Card.css'
import Pagination from "./Pagination";
import InfoMessage from "./InfoMessage";
import qs from 'query-string';
import {withRouter} from 'react-router-dom';
import CurrencyDropdown from "./CurrencyDropdown";

class Card extends Component {
    constructor(props) {
        super(props);

        this.state = {
            activePage: 0,
            itemFilter: this.props.filterItems,
        };
    }

    componentWillReceiveProps(nextProps) {
        if (this.props.filterItems !== nextProps.filterItems) {
            this.setState({
                filterItems: nextProps.filterItems,
            });
        }
    }

    getPageItems = (page) => {
        this.setState({
            activePage: page
        })

        let filter = {
            itemFilter: this.state.itemFilter,
            "page": page,
            "size": 4
        }

        this.props.getItemsForPage('http://localhost:8888/api/v1/items/getItemsForPageByFilter', filter);

    }

    changeView = () => {
        let element = document.getElementById("product-card");
        element.classList.toggle('list');
    }

    render() {
        if(!this.props.searchChanged) {
            return (
                <InfoMessage style={{width: '70%'}} message="Please enter valid search criteria to get results." />
            )
        } else if(this.props.filteredItemsCount===0) {
            return (
                <InfoMessage message="There are no items in database matching entered criteria"/>
            )
        }
        return (
            <div className="cg-wrapper">
                <div className="cview">
                    <p>View as:</p>
                    <a className="icon-grid" onClick={this.changeView}> </a>
                    <a className="icon-list" onClick={this.changeView}> </a>
                    {/*<div style={{float: 'right'}}>Currency</div>*/}
                    <div style={{float: 'right'}}>
                        <CurrencyDropdown/>
                    </div>
                    <br/>
                </div>

                <div className="products category" >
                    <ul id="product-card">
                        {this.props.filteredItems.map((item) => {
                           return (
                               <li key={item.id}>
                                   <div className="wrapimg">
                                       <img src="https://static.fnac-static.com/multimedia/Images/PT/NR/c9/2d/04/273865/1540-6/tsp20160818192157/Samsung-EC-ST500-ZBPSE1.jpg"/>
                                       <div className="addPro" onClick={() => this.props.addToCart(item)}> <h3 >+ Add to cart</h3>
                                       </div>
                                   </div>
                                   <div className="titlePro">	<a href="javascript:">{item.manufacturer} {item.name}</a>
                                       { (item.discount > 0) ?
                                           <span>
                                               ${item.price-(item.price*item.discount/100)} (<i>$<strike>{item.price}</strike></i>)
                                               <span className="discount"> <i>-{item.discount}%</i></span>
                                           </span> : <span>
                                               ${item.price}
                                           </span>
                                       }
                                       <p className="descTitle"><b>Detailed item information</b></p>
                                       <p>{item.description}</p>
                                   </div>
                               </li>
                           )
                        })}
                    </ul>
                </div>
                {this.props.filteredItemsCount!==null ? <Pagination activePage={this.state.activePage} onGetItems={this.getPageItems}
                                                                    itemsCount={this.props.filteredItemsCount/4}/> : null}
            </div>
        )
    }
}

export default withRouter(Card);