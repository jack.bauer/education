import React, { Component } from 'react';
import './SignUp.css';
import { addUserSuccess } from '../Actions/users';
import Select from './Select';
import Input from '../Components/Input';
import axios from 'axios';
import SuccessModal from '../Components/SuccessModal';
import ErrorModal from '../Components/ErrorModal';
import {withRouter} from 'react-router-dom'

class SignUp extends Component {
    constructor(props) {
        super(props);

        this.state = {
            username: '',
            password: '',
            confirmPassword: '',
            email: '',
            selectedRole: '',
            role: {},
            successAlert: null
        };
    }

    onUsernameChanged = (text) => {
        this.setState({
            username: text,
        })
    }

    onEmailChanged = (text) => {
        this.setState({
            email: text,
        })
    }

    onPasswordChanged = (text) => {
        this.setState({
            password: text,
        })
    }

    onConfirmPasswordChanged = (text) => {
        this.setState({
            confirmPassword: text,
        })
    }

    onRoleChanged = (text) => {
        let obj = {};

        let keys = Object.keys(this.props.roles);
        keys.map((key) => {
            if(this.props.roles[key].name===text){
                obj.id = this.props.roles[key].id;
            }
        })

        this.setState({
            role: obj,
            selectedRole: text,
        })

    }

    closeSuccess = () => {
        this.setState({
            alert: null
        });

        this.props.history.push('/');

    }

    closeFailed = () => {
        this.setState({
            alert: null
        });
    }

    signUp = (e) => {
        e.preventDefault(e);

        const user = {
            username: this.state.username,
            password: this.state.password,
            email: this.state.email,
            role: this.state.role,
        }

        if((this.state.role.id!==4) && (this.state.password===this.state.confirmPassword)){
            axios.put('http://localhost:8888/api/v1/users/addUser', user)
                .then((response) => {
                    this.setState({
                        alert: (
                            <SuccessModal title="Registration success" body="You need to Sign In"
                                          onConfirm={() => this.closeSuccess()}/>
                        ),
                        username: '',
                        password: '',
                        email: '',
                        confirmPassword: '',
                        selectedRole: '',
                    });
                })
                .catch((response) => {
                    console.log(response);
                    this.setState({
                        alert: (
                            <ErrorModal errorText="User was not added. Please try again"
                                        onConfirm={() => this.closeFailed()}/>
                        )
                    });
                });
        }

    }

    render() {
        return (
            <div id="login-box">
                <div className="left-sign-up">
                    <h1>Sign up</h1>
                    <form onSubmit={this.signUp}>
                        <div style={{display: 'flex', flexDirection: 'column', alignItems: 'center' }}>
                            <Input className="sign-up-input" required min="6" max="16" type="text" onTextChanged={this.onUsernameChanged}
                                   field={this.state.username} name="username" placeholder="Username" />
                            <Input className="sign-up-input" required min="6" max="20" type="email" onTextChanged={this.onEmailChanged}
                                   field={this.state.email} name="email" placeholder="Email" />
                            <Input className="sign-up-input" required min="5" max="20" type="password" onTextChanged={this.onPasswordChanged}
                                   field={this.state.password} name="password" placeholder="Password" />
                            <Input className="sign-up-input" required min="5" max="20" type="password" onTextChanged={this.onConfirmPasswordChanged}
                                   field={this.state.confirmPassword} name="password" placeholder="Retype Password" />
                            <Select items={this.props.roles} onTextChanged={this.onRoleChanged} placeholder="Role"
                                    field={this.state.selectedRole}/>

                            <input style={{flex: 1, textAlign: 'center'}} type="submit" className="sign-up" value="Sign Me Up" />
                        </div>
                    </form>
                    {this.state.alert}
                </div>

                <div className="right-up">
                </div>
            </div>
        )
    }
}

export default withRouter(SignUp);