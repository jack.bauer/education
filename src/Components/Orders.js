import React, { Component } from 'react';
import './Orders.css';
import OrderItem from "./OrderItem";
import {withRouter} from 'react-router-dom';
import InfoMessage from "./InfoMessage";

class Orders extends Component {
    constructor(props) {
        super(props);
    }

    goToSearch = () => {
        this.props.history.push("search")
    }

    render() {
        return (
            <div className="container">
                <section id="order-container">
                    <div className="row" style={{marginTop: '50px'}}>
                        <div className="col-sm-6">
                            <h1 style={{float: 'left', marginLeft: '-20px', fontSize: '28px'}}>My Orders</h1>
                        </div>
                        <div className="col-sm-6">
                            <div style={{flex: 1, justifyContent: 'center', alignItems: 'center'}} className="catalog-btn" type="button" onClick={this.goToSearch}>
                                <div>Go to Catalog</div>
                                <div className="catalog-order-icon"></div>
                                {/*<p style={{float: 'left', marginLeft: '20px', marginTop: '9px'}}>Go to catalog</p> <div className="continue-shopping-icon" />*/}
                            </div>
                        </div>
                    </div>
                    <div id="order-box">
                        <div className="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                            {(this.props.orders.length > 0) ?
                                this.props.orders.map((item, index) => {
                                    return (
                                        <OrderItem key={index} index={index} item={item}/>
                                    )
                                }) : <InfoMessage style={{width: '85%'}} message="Orders history is empty"/>
                            }
                        </div>
                    </div>
                </section>
            </div>
        )

    }
}

export default withRouter(Orders);