import React, {Component} from 'react';
import SweetAlert from 'react-bootstrap-sweetalert';

class DeleteItem extends Component {
    render() {
        return (
            <SweetAlert
                warning
                showCancel
                confirmBtnText="Yes, delete it!"
                confirmBtnBsStyle="danger"
                cancelBtnBsStyle="default"
                title="Are you sure you want to delete this item?"
                onConfirm={this.props.onConfirm}
                onCancel={this.props.onCancel}
            >
            </SweetAlert>
        )
    }
}

export default DeleteItem;