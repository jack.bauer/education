import React, {PureComponent} from 'react';
import SweetAlert from 'react-bootstrap-sweetalert';

class SuccessModal extends PureComponent {
    render() {
        return (
            <SweetAlert
                success
                confirmBtnBsStyle="success"
                title={this.props.title}
                onConfirm={this.props.onConfirm}
            >
                {this.props.body}
            </SweetAlert>
        )
    }
}

export default SuccessModal;