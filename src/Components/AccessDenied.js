import React, { PureComponent } from 'react';
import './AccessDenied.css';

class AccessDenied extends PureComponent {
    goToDashboard = () => {
        this.props.history.push("dashboard")
    }

    render() {
        return (
            <div>
                <div id="denied-message-box">
                    <div style={{display: 'flex', flexDirection: 'column', alignItems: 'center' }}>
                        <div className="denied-icon"></div>
                    </div>
                    <div>
                        <h1>You can't access this page</h1>
                        <div className="denied-dashboard">
                            <p>Go to <a onClick={this.goToDashboard}>Dashboard</a></p>
                        </div>
                    </div>
                </div>
            </div>
        )

    }
}

export default AccessDenied;