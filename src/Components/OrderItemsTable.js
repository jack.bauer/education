import React, { Component } from 'react';
import './OrderItemsTable.css';

class OrderItemsTable extends Component {
    printItem = () => {
        return (
            <tbody>
            {this.props.items.map((it, index) => {
                return (
                    <tr key={index}>
                        <td>{index}</td>
                        <td>{it.category}</td>
                        <td>{it.manufacturer} {it.name}</td>
                        <td>{it.quantity}</td>
                        <td>{it.packPrice}$</td>
                    </tr>
                )
            })}
            </tbody>
        );
    }

    render() {
        return (
            <div id="order">
                <div id="order-page">
                    <table id="user">
                        <thead>
                        <tr className="items-info">
                            <th className="first">#</th>
                            <th className="second">Category</th>
                            <th className="third">Item</th>
                            <th className="fourth">Quantity</th>
                            <th className="fifth">Pack Price</th>
                        </tr>
                        </thead>
                        {this.printItem()}
                    </table>
                </div>
            </div>
        );
    }
}

export default OrderItemsTable;