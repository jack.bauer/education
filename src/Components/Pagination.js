import React, { Component } from 'react';
import './Pagination.css'
import {withRouter} from 'react-router-dom'
import qs from 'query-string';

class Pagination extends Component {
    // className="pagination-active"
    handlePageClick = (e) => {
        e.preventDefault();
        this.setState({
            activePage: e.target.name
        });
        if(e.target.name==='first') {
            this.props.history.push(this.props.location.pathname + '?page=' + 0)
        } else if(e.target.name==='last') {
            this.props.history.push(this.props.location.pathname + '?page=' + (this.props.itemsCount-1))
        } else {
            this.props.history.push(this.props.location.pathname + '?page=' + e.target.name)
        }


        if (this.props.onGetItems) {
            if(e.target.name==='first'){
                this.props.onGetItems(0);
            } else if(e.target.name==='last'){
                this.props.onGetItems(this.props.itemsCount-1);
            } else {
                this.props.onGetItems(e.target.name);
            }
        }
    }

    returnPage = () => {
        let pages = [];
        for (let i=0; i < this.props.itemsCount; i++) {
            pages.push(i);
        }

        return (
            <span className="pagination-inner">
                {pages.map((page) => {
                    if(page==qs.parse(this.props.location.search).page){
                        return (
                            <a key={page} className="pagination-active" name={page} onClick={this.handlePageClick} href="#">{page+1}</a>
                        )
                    }
                    return (
                        <a key={page} name={page} onClick={this.handlePageClick} href="#">{page+1}</a>
                    )
                })}
            </span>
        )
    }

    render() {
        return (
            <nav className="pagination-container">
                <div className="pagination">
                    <a className="pagination-newer" href="#" name="first" onClick={this.handlePageClick}>FIRST</a>
                    {this.returnPage()}
                    <a className="pagination-older" name="last" href="#" onClick={this.handlePageClick}>LAST</a>
                </div>
            </nav>
        );
    }
}

export default withRouter(Pagination);