import React, {Component} from 'react';
import SweetAlert from 'react-bootstrap-sweetalert';
import './EditUser.css'
import { Modal, Button } from 'react-bootstrap';
import RadioButton from "./RadioButton";

class EditUser extends Component {
    constructor(props) {
        super(props);

        this.state = {
            active: this.props.active,
        };
    }

    componentWillReceiveProps(nextProps) {
        if (this.props.active !== nextProps.active) {
            this.setState({
                active: nextProps.active,
            });
        }
    }

    returnStatus = (item) => {
        return item===true ? 'active' : 'disabled'
    }

    onUserStatusChanged = (val) => {
        this.setState({
            active: val,
        })
    }

    render() {
        return (
            <div>
                <Modal className="modal-container "
                       show={this.props.show}
                       onHide={this.props.close}
                       animation={false}>

                    <Modal.Body>
                        <div className="custom-icon icon-info">
                            <div className="icon-info-before"/>
                            <div className="icon-info-after"/>
                        </div>
                        <div style={{textAlign: 'center'}}>
                            <h2>Are you sure you want to update user status?</h2>
                            <div>
                                <RadioButton onRadioChanged={this.onUserStatusChanged} active={this.returnStatus(this.state.active)}/>
                            </div>
                        </div>
                        <p className="footer">
                            <span>
                                <Button style={{marginRight: '8px'}} className="btn btn-lg btn-default" onClick={this.props.close}>Cancel</Button>
                            </span>
                            <span>
                                <Button className="btn btn-lg btn-info" onClick={() => this.props.onConfirm(this.state.active)}>Save changes</Button>
                            </span>
                        </p>
                    </Modal.Body>
                </Modal>
            </div>
        )
    }
}

export default EditUser;