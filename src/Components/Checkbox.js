import React, { Component } from 'react';
import './Checkbox.css';

class Checkbox extends Component {
    constructor(props) {
        super(props);
        this.state = {
            checked: false
        }
    }

    handleClick = (e) => {
        this.setState({
            checked: e.target.checked
        });
        if (this.props.onCheckChanged) {
            this.props.onCheckChanged(e.target.checked);
        }
    }

    render() {
        return (
            <div className="boxes">
                <input type="checkbox" id={this.props.label} checked={this.state.checked} onChange={this.handleClick}/>
                <label className="label-for-check" htmlFor={this.props.label}>{this.props.label}</label>
            </div>
        )
    }
}

export default Checkbox;