import React, {Component} from 'react';
import './RadioButton.css';

class RadioButton extends Component {
    constructor(props) {
        super(props);
        this.state = {
            checked: this.props.active,
        }
    }

    handleActiveClick = (e) => {
        this.setState({
            checked: e.target.value
        });
        if (this.props.onRadioChanged) {
            this.props.onRadioChanged(e.target.value);
        }
    }

    getChecked = () => {
        return(
            <p className="btn-switch">
                <input type="radio" value='active' checked={this.state.checked==='active'} id="yes" name="switch"
                       className="btn-switch__radio btn-switch__radio_yes" onChange={this.handleActiveClick}/>
                <input type="radio" value='disabled' checked={this.state.checked==='disabled'} id="no" name="switch"
                       className="btn-switch__radio btn-switch__radio_no" onChange={this.handleActiveClick}/>
                <label htmlFor="yes" className="btn-switch__label btn-switch__label_yes"><span className="btn-switch__txt">Active</span></label>
                <label htmlFor="no" className="btn-switch__label btn-switch__label_no"><span className="btn-switch__txt">Disabled</span></label>
            </p>
        )
    }

    render() {
        return (
            <div >
                {this.getChecked()}
            </div>

        )
    }
}

export default RadioButton;