import React, {Component} from 'react';
import SweetAlert from 'react-bootstrap-sweetalert';

class ErrorModal extends Component {
    render() {
        return (
            <SweetAlert
                error
                title="Oooops!"
                confirmBtnBsStyle="danger"
                onConfirm={this.props.onConfirm}
            >
                {this.props.errorText}
            </SweetAlert>
        )
    }
}

export default ErrorModal;