import React, { Component } from 'react';
import './CartItem.css'

class CartItem extends Component {
    constructor(props) {
        super(props);

        this.state = {
            cartItemQty: this.props.cartItem.quantity,
            alert: null,
            showModal: true,
            successAlert: null,
            price: 0,
            fullPrice: 0,
            qty: {},
            defaultQty: 1,
            taxes: 5,
            shipping: 5
        };
    }

    componentWillReceiveProps(nextProps) {
        if (this.props.cartItem !== nextProps.cartItem) {
            this.setState({
                cartItemQty: nextProps.cartItem.quantity
            });
        }
    }

    onItemIncrease = () => {
        let qty = this.state.cartItemQty;
        qty++;
        this.setState({
            cartItemQty: qty,
        });

        if (this.props.onQtyChanged) {
            this.props.onQtyChanged({
                id: this.props.cartItem.item.id,
                quantity: qty,
                item: this.props.cartItem.item
            });
        }
    }

    onItemDecreased = () => {
        let qty = this.state.cartItemQty
        if(qty > 1){
            qty--;
            this.setState({
                cartItemQty: qty,
            });

            if (this.props.onQtyChanged) {
                this.props.onQtyChanged({
                    id: this.props.cartItem.item.id,
                    quantity: qty,
                    item: this.props.cartItem.item
                });
            }
        }
    }

    render() {
        return (
            <article className="product" key={this.props.index} >
                <header>
                    <a className="remove">
                        <img src="http://www.astudio.si/preview/blockedwp/wp-content/uploads/2012/08/1.jpg" alt=""/>

                        <h3 onClick={() => this.props.onDelete(this.props.cartItem.item.id) }>Remove product</h3>
                    </a>
                </header>

                <div className="content">

                    <h1>{this.props.cartItem.item.manufacturer} {this.props.cartItem.item.name}</h1>
                    <p><b>{this.props.cartItem.item.category}</b></p>
                    {this.props.cartItem.item.description}

                </div>

                <footer className="content">
                    <div className="qt-minus" type="button" onClick={this.onItemDecreased}/>
                    <span className="qt">{this.state.cartItemQty}</span>
                    <div className="qt-plus" type="button" onClick={this.onItemIncrease}/>
                    <h2 className="full-price">
                        {(this.props.cartItem.item.price-(this.props.cartItem.item.price*this.props.cartItem.item.discount/100))*this.state.cartItemQty}$
                    </h2>

                    <h2 className="price">
                        {(this.props.cartItem.item.discount > 0) ?
                            <span>
                                {this.props.cartItem.item.price-(this.props.cartItem.item.price*this.props.cartItem.item.discount/100)} (<strike><i>{this.props.cartItem.item.price}</i></strike>)
                                <i style={{fontSize: '12px'}}>-{this.props.cartItem.item.discount}%</i>
                                </span> : <span>{this.props.cartItem.item.price}$</span>
                        }
                    </h2>
                </footer>
            </article>

        )

    }
}

export default CartItem;