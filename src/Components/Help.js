import React, { PureComponent } from 'react';
import './Help.css';
import NavigationPanel from "./NavigationPanel";

class Help extends PureComponent {
    render() {
        return (
            <div>
                <NavigationPanel />
                <div id="denied-message-box">
                    <div style={{display: 'flex', flexDirection: 'column', alignItems: 'center' }}>
                        <div className="help-icon"></div>
                    </div>
                    <div>
                        <h2>Help</h2>
                        <div className="denied-dashboard">
                        </div>
                    </div>
                </div>
            </div>
        )

    }
}

export default Help;