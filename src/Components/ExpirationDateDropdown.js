import React, {Component} from 'react';
import './ExpirationDateDropdown.css';

class ExpirationDateDropdown extends Component {
    constructor(props) {
        super(props);
        this.state = {
            field: this.props.field,
        }
    }

    onChange = (event) => {
        let value = event.target.value;
        this.setState({
            field: value,
        });
        if (this.props.onTextChanged) {
            this.props.onTextChanged(value);
        }

    }

    render() {
        if(this.props.required){
            return (
                <div>
                    <input required style={this.props.style} role="combobox" list={this.props.name ? this.props.name : 'items'} autoComplete="on" placeholder={this.props.placeholder} onChange={this.onChange} value={this.props.field}/>
                    <datalist id={this.props.name ? this.props.name : 'items'}>
                        {this.props.items.map((index) => {
                            return (
                                <option key={index} value={index}> </option>
                            )
                        })}
                    </datalist>
                </div>
            )
        } else {
            return (
                <div>
                    <input style={this.props.style} role="combobox" list={this.props.name ? this.props.name : 'items'} autoComplete="on" placeholder={this.props.placeholder} onChange={this.onChange} value={this.props.field}/>
                    <datalist id={this.props.name ? this.props.name : 'items'}>
                        {this.props.items.map((index) => {
                            return (
                                <option key={index} value={index}> </option>
                            )
                        })}
                    </datalist>
                </div>
            )
        }
    }
}

export default ExpirationDateDropdown;