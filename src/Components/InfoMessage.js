import React, { PureComponent } from 'react';
import './InfoMessage.css'

class InfoMessage extends PureComponent {
    render() {
        return (
            <div style={this.props.style} className="info-box info">
                <span>
                    {this.props.message}
                </span>
            </div>
        )
    }
}

export default InfoMessage;