import React, { Component } from 'react';
import './SignIn.css'
import Input from '../Components/Input';
import {withRouter} from 'react-router-dom';
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { push } from 'react-router-redux'

class SignIn extends Component {
    constructor(props) {
        super(props);

        this.state = {
            username: "",
            password: ""
        };
    }

    onUsernameChanged = (text) => {
        this.setState({
            username: text,
        })
    }

    onPasswordChanged = (text) => {
        this.setState({
            password: text,
        })
    }

    onClickFacebook = () => {
        window.location = "https://www.facebookk.com"
    }

    onClickGoogle = () => {
        window.location = "https://plus.google.com"
    }

    login = (e) => {
        if(this.state.username.length <= 16){
            e.preventDefault(e);
            this.props.login({
                username: this.state.username,
                password: this.state.password
            })
            // this.props.history.push('dashboard');
        }
    }

    render() {
        return (
            <div id="login-box">
                <div className="left-sign-in">
                    <h1>Sign In</h1>

                    <form onSubmit={this.login}>
                        <div style={{display: 'flex', flexDirection: 'column', alignItems: 'center' }}>
                            <Input className="sign-in-input" required min="6" max="14" type="text" onTextChanged={this.onUsernameChanged} value={this.props.username}
                                   name="username" placeholder="Username or Email" />
                            <Input className="sign-in-input" required min="6" max="20" type="password" onTextChanged={this.onPasswordChanged}
                                   value={this.props.password} name="password" placeholder="Password" />
                            <input style={{flex: 1, textAlign: 'center'}} className="sign-in" type="submit" value="Sign Me In"/>
                        </div>
                    </form>
                    <p className="message">Not registered? <a href="/signUp">Create an account</a></p>
                </div>

                <div className="right-in">
                    <span className="loginwith">Sign in with<br />social network</span>

                    <button className="social-signin facebook" onClick={this.onClickFacebook} >Log in with facebook</button>
                    <button className="social-signin google" onClick={this.onClickGoogle}>Log in with Google+</button>
                </div>
                <div className="or">OR</div>
            </div>

        )
    }
}

export default withRouter(SignIn);