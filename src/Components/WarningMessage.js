import React, { PureComponent } from 'react';
import './WarningMessage.css'

class WarningMessage extends PureComponent {
    render() {
        return (
            <div className="alert-box warning">
                <span>
                    <strong>Warning!</strong> {this.props.message}.
                </span>
            </div>
        )
    }
}

export default WarningMessage;