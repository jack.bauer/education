import React, { Component } from 'react';
import CatalogSearch from '../Components/CatalogSearch';
import './Catalog.css'
import NavigationPanel from '../Components/NavigationPanel';
import WarningMessage from '../Components/WarningMessage';
import Card from "./Card";
import Loader from '../Components/Loader'

class Catalog extends Component {
    constructor(props) {
        super(props);

        this.state = {
            searchChanged: false,
            filter: {},
            emptySearch: false
        }
    }

    onFilterChanged = (val) => {
        this.setState({
            searchChanged: val,
            emptySearch: false
        })
    }

    onFilterPass = (val) => {
        this.setState({
            filter: val,
            emptySearch: false
        })
    }

    onCountChanged = (val) => {
        this.props.onLoad('http://localhost:8888/api/v1/items/getItemsCountByFilter', val);
    }

    onBlankFormSearch = (val) => {
        this.setState({
            emptySearch: val
        })
    }

    render() {
        return (
            <div>
                <NavigationPanel />
                <div className="row">
                    <div >
                        <div className="col-sm-3">
                            <CatalogSearch filterPass={this.onFilterPass} onGetCount={this.onCountChanged} onSearchChanged={this.onFilterChanged}
                                           categories={this.props.categories} onItemsSearch={(url, obj) => this.props.onSearch(url, obj)}
                                           onEmptySearch={this.onBlankFormSearch}/>
                        </div>
                        <div className="col-sm-7">
                            {this.state.emptySearch ? <WarningMessage message="Please fill in at least one search criteria"/> :
                            this.props.isLoading ? <Loader/> :
                                <Card filterItems={this.state.filter} filteredItemsCount={this.props.filteredItemsCount} searchChanged={this.state.searchChanged}
                                      filteredItems={this.props.filteredItems} getItemsForPage={(url, obj) => this.props.onSearch(url, obj)}
                                      addToCart={(obj) => this.props.onAddToCart(obj)}/>
                            }
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default Catalog;