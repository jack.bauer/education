import React, {Component} from 'react';
import './Select.css';

class Select extends Component {
    constructor(props) {
        super(props);
        this.state = {
            field: this.props.field,
        }
    }

    onChange = (event) => {
        let value = event.target.value;
        this.setState({
            field: value,
        });
        if (this.props.onTextChanged) {
            this.props.onTextChanged(value);
        }

    }

    render() {
        return (
            <div>
                <input required={this.props.required} style={this.props.style} role="combobox" list={this.props.name ? this.props.name : 'items'} autoComplete="on" placeholder={this.props.placeholder} onChange={this.onChange} value={this.props.field}/>
                <datalist id={this.props.name ? this.props.name : 'items'}>
                    {this.props.items.map((index) => {
                        return (
                            <option key={index.id} value={index.name}> </option>
                        )
                    })}
                </datalist>
            </div>
        )
    }
}

export default Select;