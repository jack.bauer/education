import React, { Component } from 'react';
import './UserTable.css';
import DeleteUser from '../Containers/DeleteUser';
import SuccessModal from '../Components/SuccessModal';
import ErrorModal from '../Components/ErrorModal';
import axios from 'axios';
import EditUser from "./EditUser";

class UserTable extends Component {
    constructor(props) {
        super(props);

        this.state = {
            alert: null,
            showModal: true,
            successAlert: null,
        };
    }

    returnStatus = (status) => {
        return (status==='active') ? true : (status==='disabled') ? false : null
    }

    callDeleteUser(val) {
        const getAlert = () => (
            <DeleteUser user={val} onConfirm={() => this.deleteUser(val)} onCancel={() => this.cancelAlert()}/>
        );

        this.setState({
            alert: getAlert()
        });
    }

    callEditUser(val) {
        const getAlert = () => (
            <EditUser active={val.active} user={val} show={this.state.showModal} onConfirm={(status) => {this.updateUserStatus(val, status)}} close={this.cancelAlert}/>
        );

        this.setState({
            alert: getAlert()
        });
    }

    deleteUser(ob) {
        axios.delete('http://localhost:8888/api/v1/users/deleteUser/' + ob.id)
            .then((response) => {
                console.log(response);
                this.setState({
                    alert: (
                        <SuccessModal title="Success" onConfirm={() => this.cancelAlert()}/>
                    )
                });
            })
            .catch((response) => {
                console.log(response);
                this.setState({
                    alert: (
                        <ErrorModal errorText="Case was not deleted. Please try again" onConfirm={() => this.cancelAlert()}/>
                    )
                });
            });

        this.setState({
            alert: null
        });
    }

    updateUserStatus(ob, status) {
        let boolStatus = this.returnStatus(status);
        axios.put('http://localhost:8888/api/v1/users/updateUserStatus/' + ob.id + '/' + boolStatus)
            .then((response) => {
                console.log(response);
                this.setState({
                    alert: (
                        <SuccessModal title="Success" onConfirm={() => this.cancelAlert()}/>
                    )
                });
            })
            .catch((response) => {
                console.log(response);
                this.setState({
                    alert: (
                        <ErrorModal errorText="User was not updated. Please try again" onConfirm={() => this.cancelAlert()}/>
                    )
                });
            });

        this.setState({
            alert: null
        });
    }

    cancelAlert = () => {
        this.setState({
            alert: null
        });
    }

    returnUsertStatus = (bool) => {
        return bool ? 'ACTIVE' : 'DISABLED'
    }

    printUser = () => {
        let i = 1;
        return (
            <tbody>
            {this.props.users.map((user) => {
                return (
                    <tr key={user.id}>
                        <td>{i++}</td>
                        <td>{user.username}</td>
                        <td>{user.email}</td>
                        <td>
                            <span key={user.role.id}>{user.role.name}</span>
                        </td>
                        <td>{this.returnUsertStatus(user.active)}</td>
                        <td>
                            <div className="edit" onClick={() => this.callEditUser(user)}></div>
                        </td>
                        <td>
                            <div className="trash" onClick={() => this.callDeleteUser(user)}></div>
                        </td>
                    </tr>
                )
            })}
            </tbody>
        );
    }

    render() {
        return (
            <div id="wu">
                <div id="user-page">
                    <table id="user">
                        <thead>
                        <tr className="userinfo">
                            <th className="first">#</th>
                            <th className="second">Username</th>
                            <th className="third">Email</th>
                            <th className="fourth">Role</th>
                            <th className="fifth">Status</th>
                            <th className="six">&nbsp;</th>
                            <th className="seven">&nbsp;</th>
                        </tr>
                        </thead>
                        {this.printUser()}
                    </table>
                </div>
                {this.state.alert}
            </div>
        );
    }
}

export default UserTable;