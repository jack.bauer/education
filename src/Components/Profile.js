import React, { Component } from 'react';
import './Profile.css'
import Input from '../Components/Input';
import { updateUserProfileSuccess } from '../Actions/profile';
import axios from 'axios';
import SuccessModal from '../Components/SuccessModal';
import ErrorModal from '../Components/ErrorModal';
import InfoModal from '../Components/InfoModal';
import {withRouter} from 'react-router-dom'
import ImgCropper from "./ImgCropper";

class Profile extends Component {
    constructor(props) {
        super(props);

        this.state = {
            id: this.props.userProfile.id,
            firstname: this.props.userProfile.firstname,
            lastname: this.props.userProfile.lastname,
            email: this.props.userProfile.email,
            profilePhoto: this.props.userProfile.profilePhoto,
            role: this.props.userProfile.role,
            userInfo: this.props.userProfile,
            changed: false,
            successAlert: null,
            selectedProfilePhoto: null
        };

    }

    componentWillReceiveProps(nextProps) {
        if (this.props.userProfile !== nextProps.userProfile) {
            this.setState({
                id: nextProps.userProfile.id,
                firstname: nextProps.userProfile.firstname,
                lastname: nextProps.userProfile.lastname,
                email: nextProps.userProfile.email,
                profilePhoto: nextProps.userProfile.profilePhoto,
                role: nextProps.userProfile.role,
                userInfo: nextProps.userProfile,
            });
        }
    }


    onEmailChanged = (text) => {
        this.setState({
            email: text,
        })
    }

    onLastNameChanged = (text) => {
        this.setState({
            lastname: text,
        })
    };

    onFirstNameChanged = (text) => {
        this.setState({
            firstname: text,
        })
    };

    closeSuccess = () => {
        this.setState({
            alert: null
        });

    }

    closeFailed = () => {
        this.setState({
            alert: null
        });
    }

    confirmClose = () => {
        this.setState({
            alert: null
        });
        this.props.history.goBack();
    }

    profilePhotoUpdate = (val) => {
        this.setState({
            selectedProfilePhoto: val
        });
    }

    updateProfile = (e) => {
        e.preventDefault(e);

        let fileURL = null;

        const profile = {
            id: this.state.id,
            firstname: this.state.firstname,
            lastname: this.state.lastname,
            email: this.state.email,
        }

        if(this.state.selectedProfilePhoto === null) {
            axios.put('http://localhost:8888/api/v1/users/updateUserProfile', profile)
                .then((response) => {
                    this.setState({
                        alert: (
                            <SuccessModal title="Success" body="User profile was updated successfully"
                                          onConfirm={() => this.closeSuccess()}/>
                        )
                    });
                })
                .catch((response) => {
                    console.log(response);
                    this.setState({
                        alert: (
                            <ErrorModal errorText="Profile was not updated. Please try again"
                                        onConfirm={() => this.closeFailed()}/>
                        )
                    });
                });
        } else {
            const formData = new FormData();
            formData.append("file", this.state.selectedProfilePhoto);
            formData.append("tags", 'profile');
            formData.append("upload_preset", "konylk0s");
            formData.append("api_key", "256883859555725");
            formData.append("timestamp", (Date.now() / 1000) | 0);

            axios.post("https://api.cloudinary.com/v1_1/dxp2voatn/image/upload", formData, {
                headers: { "X-Requested-With": "XMLHttpRequest" },
            }).then(response => {
                const data = response.data;
                fileURL = data.secure_url

                profile.profilePhoto = fileURL;

                axios.put('http://localhost:8888/api/v1/users/updateUserProfile', profile)
                    .then((response) => {
                        this.setState({
                            alert: (
                                <SuccessModal title="Success" body="User profile was updated successfully"
                                              onConfirm={() => this.closeSuccess()}/>
                            )
                        });
                    })
                    .catch((response) => {
                        console.log(response);
                        this.setState({
                            alert: (
                                <ErrorModal errorText="Profile was not updated. Please try again"
                                            onConfirm={() => this.closeFailed()}/>
                            )
                        });
                    });
            })
        }

    }

    cancel = () => {
        let firstname, lastname;

        if(this.state.firstname===""){
            firstname = null
        } else {
            firstname = this.state.firstname;
        }

        if(this.state.lastname===""){
            lastname = null;
        } else {
            lastname = this.state.lastname;
        }

        const profile = {
            id: this.state.id,
            firstname: firstname,
            lastname: lastname,
            profilePhoto: this.state.profilePhoto,
            email: this.state.email,
            role: this.state.role,
        }

        if(this.isEquivalent(profile, this.state.userInfo)){
            this.props.history.goBack();
        } else {
            this.setState({
                alert: (
                    <InfoModal infoText="Do you want cancel changes?"
                                onConfirm={() => this.confirmClose()}
                                onCancel={() => this.closeSuccess()}/>
                )
            });
        }
    }

    isEquivalent = (a, b) => {
        let aProps = Object.getOwnPropertyNames(a);
        let bProps = Object.getOwnPropertyNames(b);

        if (aProps.length !== bProps.length) {
            return false;
        }

        for (let i = 0; i < aProps.length; i++) {
            let propName = aProps[i];

            if (a[propName] !== b[propName]) {
                return false;
            }
        }

        return true;
    }

    render() {
        return (
            <div id="profile-box">
                <div className="left-profile">
                    <h1>Profile</h1>
                    <form onSubmit={this.updateProfile}>
                        <Input className="profile-input" min="5" max="22" type="text" onTextChanged={this.onFirstNameChanged}
                               field={this.state.firstname || ''} placeholder="First Name" restrictType="number"/>
                        <Input className="profile-input" min="7" max="22" type="text" onTextChanged={this.onLastNameChanged}
                               field={this.state.lastname || ''} placeholder="Last Name" restrictType="number"/>
                        <Input className="profile-input" required min="6" max="18" type="email" onTextChanged={this.onEmailChanged}
                               field={this.state.email || ''} name="email" placeholder="Email" />
                        <Input className="profile-input" type="text" placeholder={this.state.role} disabled={true} />

                        <div className="row profile-row-button">
                            <div className="col-sm-6">
                                <div style={{flex: 1, justifyContent: 'center', alignItems: 'center'}} className="cancel-changes" type="button" onClick={this.cancel}>
                                    <div>Cancel</div>
                                    <div className="cancel-changes-icon"></div>
                                    {/*<p style={{float: 'left', marginLeft: '20px', marginTop: '9px'}}>Go to catalog</p> <div className="continue-shopping-icon" />*/}
                                </div>
                            </div>
                            <div className="col-sm-6" style={{paddingLeft: 0}}>
                                <button style={{flex: 1, justifyContent: 'center', alignItems: 'center'}} className="submit-profile" type="submit" >
                                    <div>Save</div>
                                    <div className="submit-profile-icon"></div>
                                </button>
                            </div>
                        </div>
                    </form>
                    {this.state.alert}
                </div>

                <div className="right-in-profile">
                    <h1 >Select photo</h1>
                    <div className="profile-select">
                        <ImgCropper classname="avatar-profile-photo" photo={this.state.profilePhoto} onImageCropped={this.profilePhotoUpdate}/>
                    </div>

                </div>
                <div className="or-profile"> and </div>
            </div>
        )
    }
}

export default withRouter(Profile);