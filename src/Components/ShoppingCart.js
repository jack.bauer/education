import React, { Component } from 'react';
import NavigationPanel from './NavigationPanel';
import './ShoppingCart.css';
import Checkout from "./Checkout";
import {withRouter} from 'react-router-dom';
import InfoMessage from "./InfoMessage";
import CartItem from "./CartItem";
import SuccessModal from '../Components/SuccessModal';
import ErrorModal from '../Components/ErrorModal';
import Checkbox from "./Checkbox";
import Input from "./Input";
import InfoModal from '../Components/InfoModal';

class ShoppingCart extends Component {
    constructor(props) {
        super(props);

        this.state = {
            cartItems: this.props.cartItems,
            filteredCartItems: this.getUniqueItems(props),
            cartItemsForPurchase: [],
            alert: null,
            showModal: true,
            showPaymentStatus: null,
            showPaymentError: false,
            price: 0,
            subTotal: this.getSubTotal(props),
            qty: {},
            defaultQty: 1,
            taxes: 5,
            shipping: 0,
            shippingTrue: false,
            discountCode: '',
            userRole: this.props.user.role
        };
    }

    componentWillReceiveProps(nextProps) {
        if (this.props.cartItems !== nextProps.cartItems) {
            this.setState({
                filteredCartItems: this.getUniqueItems(nextProps),
                subTotal: this.getSubTotal(nextProps),
            });
        }

        if (this.props.purchaseStatus !== nextProps.purchaseStatus) {
            if(nextProps.purchaseStatus.purchaseSuccess){
                this.setState({
                    showPaymentStatus: (
                        <SuccessModal title="Payment Success" onConfirm={() => this.confirmSuccessPayment()}/>
                    )
                });
            } else {
                this.setState({
                    showPaymentStatus: (
                        <ErrorModal errorText="Payment not accepted. Please try again" onConfirm={() => this.cancelAlert()}/>
                    )
                });
            }
        }
    }

    goToSearch = () => {
        this.props.history.push("search")
    }

    callCheckout = () => {
        for(let i in this.state.filteredCartItems){
            this.setState({
                cartItemsForPurchase: this.state.cartItemsForPurchase.push(this.getItemsToCheckout(this.state.filteredCartItems[i]))
            });
        }

        const getAlert = () => (
            <Checkout show={this.state.showModal} close={this.cancelAlert} itemsToPurchase={this.state.cartItemsForPurchase} onPurchase={(obj) => this.props.onCheckout(obj)}/>
        );

        this.setState({
            alert: getAlert()
        });

    }

    cancelAlert = () => {
        this.setState({
            alert: null,
            showPaymentStatus: null,
            cartItemsForPurchase: []
        });
    }

    confirmSuccessPayment = () => {
        this.setState({
            showPaymentStatus: null
        });

        this.props.history.push("orders")
    }

    getItemsToCheckout = (ob) => {
        let itemObj = {};
        itemObj.id = ob.item.id
        itemObj.quantity = ob.quantity

        return itemObj;
    }

    getUniqueItems = (props) => {
        let input = props.cartItems;

        let result = input.reduce((map, item) => {
            let itemArray = map[item.id];
            if (!itemArray) {
                itemArray = {quantity: 0, item: item};
            }
            itemArray.quantity = itemArray.quantity + 1;
            map[item.id] = itemArray;
            return map;
        }, {})

        return result;
    }

    itemsArray = (obj) => {
        let res = [];
        for(let i in obj){
            res.push(obj[i])
        }

        return res;
    }

    getSubTotal = (props) => {
        let input = props.cartItems;
        let subTotal = 0;

        let result = input.reduce((map, item) => {
            let itemArray = map[item.id];
            if (!itemArray) {
                itemArray = {quantity: 0, item: item};
            }
            itemArray.quantity = itemArray.quantity + 1;
            map[item.id] = itemArray;
            return map;
        }, {})

        for(let i in result){
            subTotal += (result[i].item.price-(result[i].item.price*result[i].item.discount/100))
            // subTotal += result[i].item.price*result[i].quantity
        }

        // console.log(subTotal)

        return subTotal;
    }

    getUpdatedItem = (val) => {
        let total = 0;
        let obj = {};
        this.setState({
            filteredCartItems: Object.assign({}, this.state.filteredCartItems, {
                [val.id]: val
            }),
        });

        obj = Object.assign({}, this.state.filteredCartItems, {
            [val.id]: val
        })

        for(let i in obj){
            total += (obj[i].item.price-(obj[i].item.price*obj[i].item.discount/100))*obj[i].quantity
        }

        this.setState({
            subTotal: total
        });

        // this.props.onCartQtyChanged(val)
        //
        // console.log(this.state.filteredCartItems)
    }

    getTaxes = () => {
        return (this.state.subTotal*this.state.taxes/100);
    }

    getTotal = () => {
        return this.state.subTotal + this.getTaxes() + this.state.shipping
    }

    onShippingChanged = (val) => {
        this.setState({
            shippingTrue: val,
        })

        if((val) &&  this.state.userRole!=='premium_buyer'){
            this.setState({
                shipping: 30,
            })
        } else {
            this.setState({
                shipping: 0,
            })
        }

    }

    onDiscountCodeChanged = (val) => {
        this.setState({
            discountCode: val,
        })
    }

    applyDiscount = () => {
        if(this.state.discountCode==='godel'){
            this.setState({
                subTotal: this.state.subTotal - (this.state.subTotal*3/100),
            })
        } else if (this.state.discountCode==='tech'){
            this.setState({
                subTotal: this.state.subTotal - (this.state.subTotal*5/100),
            })
        }
    }

    confirmRemoving = (id) => {
        this.setState({
            alert: null
        });
        this.props.onDelete(id)
    }

    cancelRemoving = () => {
        this.setState({
            alert: null
        });
    }

    removeItemFromCart = (id) => {
        let itemId = id;
        this.setState({
            alert: (
                <InfoModal infoText="Are you sure you want to remove this item from cart?"
                           onConfirm={(id) => this.confirmRemoving(itemId)}
                           onCancel={() => this.cancelRemoving()}/>
            )
        });
    }

    render() {
        return (
            <div>
                <NavigationPanel />
                <div>
                    <div className="container">
                        <section id="cart">
                            <div className="row" style={{marginTop: '50px'}}>
                                <div className="col-sm-6">
                                    <h1 style={{float: 'left', marginLeft: '-20px', fontSize: '28px'}}>My Shopping Cart</h1>
                                </div>
                                <div className="col-sm-6">
                                    <div style={{flex: 1, justifyContent: 'center', alignItems: 'center'}} className="catalog-btn" type="button" onClick={this.goToSearch}>
                                        <div>Continue shopping</div>
                                        <div className="catalog-order-icon"></div>
                                    </div>
                                </div>
                            </div>

                            { (this.itemsArray(this.state.filteredCartItems).length> 0) ?
                                this.itemsArray(this.state.filteredCartItems).map((item, index) => {
                                    return (
                                        <CartItem key={index} cartItem={item} onQtyChanged={this.getUpdatedItem} onDelete={(id) => this.removeItemFromCart(id)}/>
                                    )
                                }) : <InfoMessage style={{width: '80%'}} message="There are no items in your cart"/>
                            }
                        </section>
                        <div className="row custom-row">
                            <div className="col-sm-6 discount-col">
                                <div className="col-sm-2"/>
                                <div className="col-sm-10" style={{textAlign: 'left'}}>
                                    <div className="row custom-row" style={{paddingLeft: '30px'}}>
                                        <h4>I have a coupon:</h4>
                                    </div>
                                    <div className="row custom-discount-row" style={{paddingLeft: '15px'}}>
                                        <div className="col-sm-5">
                                            <Input className="discount-input" type="text" onTextChanged={this.onDiscountCodeChanged} field={this.state.discountCode}
                                                   placeholder="Coupon code" max="5"/>
                                            <div>
                                                <Checkbox onCheckChanged={this.onShippingChanged} label="Need Shipping"/>
                                            </div>
                                        </div>
                                        <div className="col-sm-7">
                                            <div style={{flex: 1, justifyContent: 'center', alignItems: 'center'}} className="coupon-cart-btn" type="button" onClick={this.applyDiscount}>
                                                <div>Apply coupon</div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <footer id="site-footer">
                        <div style={{width: '1000px'}} className="container clearfix">
                            <div className="row custom-row">
                                <div className="col-sm-6" style={{textAlign: 'left'}}>
                                    <h2 className="subtotal">Subtotal: <span>{this.state.subTotal}</span>$</h2>
                                    <h3 className="tax">Taxes (5%): <span>{this.getTaxes().toFixed(2)}</span>$</h3>
                                    {((this.state.shippingTrue) && (this.state.userRole!=='premium_buyer')) ?
                                        <h3 className="shipping">Shipping: <span>{this.state.shipping}</span>$</h3> :
                                        ((this.state.shippingTrue) && (this.state.userRole==='premium_buyer')) ? <h3 className="shipping">Shipping: free shipping</h3> :
                                        <h3 className="shipping">Shipping: no shipping</h3>}
                                </div>

                                <div className="col-sm-6 right">
                                    <h1 className="total">Total: <span>{this.getTotal().toFixed(2)}</span>$</h1>
                                    <div style={{flex: 1, justifyContent: 'center', alignItems: 'center'}} className="checkout-cart-btn" type="button" onClick={this.callCheckout}>
                                        <div>Checkout</div>
                                        <div className="checkout-cart-icon"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </footer>
                    {this.state.alert}
                    {this.state.showPaymentStatus}
                </div>
            </div>
        );
    }
}

export default withRouter(ShoppingCart);