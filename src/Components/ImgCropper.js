import React, { Component } from 'react';
import AvatarCropper from "react-avatar-cropper";
import FileUpload from './FileUpload';
import './ImgCropper.css';
import ImageUploader from 'react-images-upload';

class ImgCropper extends Component {
    constructor(props) {
        super(props);
        this.state = {
            cropperOpen: false,
            img: null,
            croppedImg: this.props.photo
        }
    }

    componentWillReceiveProps(nextProps) {
        if (this.props.photo !== nextProps.photo) {
            this.setState({
                croppedImg: nextProps.photo
            });
        }
    }

    handleFileChange = (dataURI) => {
        this.setState({
            img: dataURI,
            croppedImg: this.state.croppedImg,
            cropperOpen: true
        });
    }

    handleCrop = (dataURI) => {
        this.setState({
            cropperOpen: false,
            img: null,
            croppedImg: dataURI
        });

        if (this.props.onImageCropped) {
            this.props.onImageCropped(dataURI);
        }
    }

    handleRequestHide = () => {
        this.setState({
            cropperOpen: false
        });
    }

    render() {
        return (
            <div style={this.props.style}>
                <div className={this.props.classname}>
                    <FileUpload handleFileChange={this.handleFileChange} />
                    <div className="avatar-edit">
                        <span>Click to Pick Product image</span>
                    </div>
                    <img src={this.state.croppedImg} />
                </div>
                {this.state.cropperOpen &&
                <AvatarCropper
                    onRequestHide={this.handleRequestHide}
                    cropperOpen={this.state.cropperOpen}
                    onCrop={this.handleCrop}
                    image={this.state.img}
                    width={400}
                    height={400}
                />
                }
            </div>
        )
    }

}

export default ImgCropper;