import React, {Component} from 'react';
import './EditItem.css'
import { Modal, Button } from 'react-bootstrap';
import Input from "./Input";
import TextArea from "./TextArea";
import RadioButton from "./RadioButton";

class EditItem extends Component {
    constructor(props) {
        super(props);

        this.state = {
            category: this.props.item.category,
            manufacturer: this.props.item.manufacturer,
            name: this.props.item.name,
            description: this.props.item.description,
            price: this.props.item.price,
            discount: this.props.item.discount,
            quantity: this.props.item.quantity,
            active: this.props.active,
        };
        console.log(this.state.category)
    }

    componentWillReceiveProps(nextProps) {
        if (this.props.item !== nextProps.item) {
            this.setState({
                category: this.props.item.category,
                manufacturer: nextProps.item.manufacturer,
                name: nextProps.item.name,
                description: nextProps.item.description,
                price: nextProps.item.price,
                discount: nextProps.item.discount,
                quantity: nextProps.item.quantity,
                active: nextProps.active,
            });
        }
    }
    //
    onNameChanged = (text) => {
        this.setState({
            name: text,
        })
    }

    onManufacturerChanged = (text) => {
        this.setState({
            manufacturer: text,
        })
    }

    onDescriptionChanged = (text) => {
        this.setState({
            description: text,
        })
    }

    onPriceChanged = (text) => {
        this.setState({
            price: text,
        })
    }

    onDiscountChanged = (text) => {
        this.setState({
            discount: text,
        })
    }

    onQuantityChanged = (text) => {
        this.setState({
            quantity: text,
        })
    }

    onItemStatusChanged = (val) => {
        this.setState({
            active: val,
        })
    }

    returnStatus = (item) => {
        return item===true ? 'active' : 'disabled'
    }

    getFinalStatus = () => {
        return (this.state.active==='active') ? true : (this.state.active==='disabled') ? false : (this.state.active===true) ? true : null
    }

    getUpdates = () => {
        return {
            category: this.state.category,
            name: this.state.name,
            manufacturer: this.state.manufacturer,
            description: this.state.description,
            price: this.state.price,
            discount: this.state.discount,
            quantity: this.state.quantity,
            active: this.getFinalStatus()
        }
    }

    render() {
        return (
            <div>
                <Modal className="modal-container edit-item-modal"
                       show={this.props.show}
                       onHide={this.props.close}
                       animation={false}>
                    <form onSubmit={() => this.props.onConfirm(this.getUpdates())}>
                        <Modal.Body>
                            <div style={{textAlign: 'center'}}>
                                <h2>Update item</h2>
                            </div>
                            <div className="row custom-row prod-item">
                                <div className="col-sm-4">
                                    <label htmlFor="category">Category</label>
                                    <Input className="edit-item-input" style={{width: '150px'}} name="category" type="text" field={this.props.item.category} disabled/>
                                </div>
                                <div className="col-sm-4">
                                    <label htmlFor="manufacturer">Manufacturer</label>
                                    <Input className="edit-item-input" style={{width: '150px'}} name="manufacturer" required min="6" max="20" type="text" onTextChanged={this.onManufacturerChanged}
                                           field={this.state.manufacturer} placeholder="Manufacturer"/>
                                </div>
                                <div className="col-sm-4">
                                    <label htmlFor="name">Name</label>
                                    <Input className="edit-item-input" style={{width: '150px'}} name="name" min="6" max="20" required type="text" onTextChanged={this.onNameChanged}
                                           field={this.state.name} placeholder="Name"/>
                                </div>
                            </div>
                            <div className="row custom-row prod-item">
                                <div className="col-sm-12">
                                    <label htmlFor="description">Description</label>
                                    <TextArea name="description" style={{width: '100%'}} placeholder="Description" rowSize="2" max="64"
                                              onTextChanged={this.onDescriptionChanged} field={this.state.description}/>
                                </div>
                            </div>
                            <div className="row custom-row" style={{verticalAlign: 'bottom'}}>
                                <div className="col-sm-3 prod-item" style={{paddingRight: 0}}>
                                    <label htmlFor="price">Price $</label>
                                    <Input className="edit-item-input" name="price" type="text" style={{width: '80px'}} restrictType="text" min="2" max="8"
                                           onTextChanged={this.onPriceChanged} field={this.state.price} placeholder="Price"/>
                                </div>
                                <div className="col-sm-3 prod-item" style={{padding: 0}}>
                                    <label htmlFor="discount">Discount %</label>
                                    <Input className="edit-item-input" name="discount" style={{width: '70px'}} max="2" type="text" onTextChanged={this.onDiscountChanged} field={this.state.discount}
                                           placeholder="Discount %" restrictType="text"/>
                                </div>
                                <div className="col-sm-3 prod-item" style={{paddingLeft: 0, width: '20%'}}>
                                    <label htmlFor="quantity">Quantity</label>
                                    <Input className="edit-item-input" required name="quantity" style={{width: '70px'}} min="0" max="4" type="text" onTextChanged={this.onQuantityChanged}
                                           field={this.state.quantity} placeholder="Quantity" restrictType="text"/>
                                </div>
                                <div className="col-sm-3" style={{paddingLeft: 0}}>
                                    <label htmlFor="quantity" className="shelf-radio">Item on shelf</label>
                                    <RadioButton onRadioChanged={this.onItemStatusChanged} active={this.returnStatus(this.state.active)}/>
                                </div>
                            </div>
                            <p className="footer">
                                <span>
                                    <Button style={{marginRight: '8px'}} className="btn btn-lg btn-default" onClick={this.props.close}>Cancel</Button>
                                </span>
                                <span>
                                    <Button className="btn btn-lg btn-info"  type="submit">Save changes</Button>
                                </span>
                            </p>
                        </Modal.Body>
                    </form>
                </Modal>
            </div>
        )
    }
}

export default EditItem;