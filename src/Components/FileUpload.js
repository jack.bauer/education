import React, { Component } from 'react';
import ReactDom from "react-dom";

class FileUpload extends Component {
    handleFile = (event) => {
        let input = event.target;

        let reader = new FileReader();
        reader.onload = () => {
            let dataURL = reader.result;
            // let output = document.getElementById('output');
            // output.src = dataURL;
            this.props.handleFileChange(dataURL);
        };
        reader.readAsDataURL(input.files[0]);
    };

    render() {
        return (
            <input id="output" type="file" accept="image/*" onChange={this.handleFile} />
        );
    }
}

export default FileUpload;