import React, {Component} from 'react';
import SweetAlert from 'react-bootstrap-sweetalert';

class InfoModal extends Component {
    render() {
        return (
            <SweetAlert
                warning
                showCancel
                title="Confirm!"
                confirmBtnBsStyle="success"
                cancelBtnBsStyle="default"
                onConfirm={this.props.onConfirm}
                onCancel={this.props.onCancel}
            >
                {this.props.infoText}
            </SweetAlert>
        )
    }
}

export default InfoModal;