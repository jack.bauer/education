import React, {Component} from 'react';
import './CardShelf.css'
import Pagination from "./Pagination";
import {withRouter} from 'react-router-dom';
import qs from 'query-string';
import InfoMessage from "./InfoMessage";
import SuccessModal from "./SuccessModal";
import ErrorModal from "./ErrorModal";
import axios from 'axios';
import DeleteItem from "./DeleteItem";
import EditItem from './EditItem';

class CardShelf extends Component {
    constructor(props) {
        super(props);

        this.state = {
            alert: null,
            showModal: true,
            successAlert: null,
            userId: this.props.userId,
        };
    }

    componentWillReceiveProps(nextProps) {
        if (this.props.userId !== nextProps.userId) {
            this.setState({
                userId: nextProps.userId,
            });
        }
    }

    componentDidMount(){
        let pageNum = qs.parse(this.props.location.search);
        if(!pageNum.page) {
            if(this.props.itemType==='general'){
                this.props.getItemsForPage('http://localhost:8888/api/v1/items/filterItemsBySupplier/'+ this.props.userId + '/0/4');
            } else if(this.props.itemType==='zeroQty'){
                this.props.getItemsForPage('http://localhost:8888/api/v1/items/filterZeroItemsBySupplier/'+ this.props.userId + '/0/4');
            } else if(this.props.itemType==='disabled'){
                this.props.getItemsForPage('http://localhost:8888/api/v1/items/filterDisabledItemsBySupplier/'+ this.props.userId + '/0/4');
            }
            this.props.history.push('/goods?page=0')
        } else {
            if(this.props.itemType==='general') {
                this.props.getItemsForPage('http://localhost:8888/api/v1/items/filterItemsBySupplier/' +  this.props.userId + '/' + pageNum.page + '/4');
            } else if(this.props.itemType==='zeroQty'){
                this.props.getItemsForPage('http://localhost:8888/api/v1/items/filterZeroItemsBySupplier/' +  this.props.userId + '/' + pageNum.page + '/4');
            } else if(this.props.itemType==='disabled'){
                this.props.getItemsForPage('http://localhost:8888/api/v1/items/filterDisabledItemsBySupplier/' +  this.props.userId + '/' + pageNum.page + '/4');
            }
        }
    }

    getItems = () => {
        let pageNum = qs.parse(this.props.location.search);
        if(this.props.itemType==='general'){
            this.props.getItemsForPage('http://localhost:8888/api/v1/items/filterItemsBySupplier/' +  this.props.userId + '/' + pageNum.page + '/4');
        } else if(this.props.itemType==='zeroQty'){
            this.props.getItemsForPage('http://localhost:8888/api/v1/items/filterZeroItemsBySupplier/' +  this.props.userId + '/' + pageNum.page + '/4');
        } else if(this.props.itemType==='disabled'){
            this.props.getItemsForPage('http://localhost:8888/api/v1/items/filterDisabledItemsBySupplier/' +  this.props.userId + '/' + pageNum.page + '/4');
        }
    }

    callEditItem = (val) => {
        const getAlert = () => (
            <EditItem cur={this.state.selectedCurrency} item={val} show={this.state.showModal} active={val.active}
                      onConfirm={(ob) => {this.updateItem(val, ob)}} close={() => this.cancelAlert()}/>
        );

        this.setState({
            alert: getAlert()
        });
    }

    updateItem(val, ob) {
        ob.category = {
            name: val.category
        }

        ob.id = val.id

        axios.post('http://localhost:8888/api/v1/items/editItem', ob)
            .then((response) => {
                // console.log(response);
                this.setState({
                    alert: (
                        <SuccessModal title="Success" onConfirm={() => this.cancelAlert()}/>
                    )
                });
            })
            .catch((response) => {
                console.log(response);
                this.setState({
                    alert: (
                        <ErrorModal errorText="Item was not updated. Please try again" onConfirm={() => this.cancelAlert()}/>
                    )
                });
            });

        this.setState({
            alert: null
        });
        // console.log(val.category.id)
        // console.log(ob)
    }

    cancelAlert = () => {
        this.setState({
            alert: null
        });
    }

    render() {
        if(this.props.items.length===0){
            return (
                <div>
                    <InfoMessage style={{width: '50%'}} message="There are no items on your shelf."/>
                </div>
            )
        }
        return (
            <div className="container">
                <section id="cart">
                    {/*<WarningMessage/>*/}
                    {/*<ul id="product-card" className="list">*/}
                        {this.props.items.map((item) => {
                            return (
                                <article className="product" key={item.id} >
                                    <header>
                                        <a className="remove">
                                            <img src="http://www.astudio.si/preview/blockedwp/wp-content/uploads/2012/08/1.jpg" alt=""/>
                                        </a>
                                    </header>
                                    <div className="content">
                                        {(this.props.itemType==='disabled') ?
                                             <h1>{item.manufacturer} {item.name} <i style={{color: 'red', fontSize: '16px'}}>(disabled)</i></h1>
                                             : (item.active===false) ? <h1>{item.manufacturer} {item.name} <i style={{color: 'red', fontSize: '16px'}}>(disabled)</i></h1>
                                                : <h1>{item.manufacturer} {item.name}</h1>
                                        }

                                        <p><b>{item.category}</b></p>
                                        {item.description}
                                        <div className="editRem">
                                            <div className="editItem" onClick={() => this.callEditItem(item)}></div>
                                        </div>
                                    </div>

                                    <footer className="content">
                                        {/*<div className="qt-minus" type="button" onClick={this.onItemDecreased}/>*/}
                                        <span className="shelf-qt"><b>Qty in storage:</b> {item.quantity}</span>
                                        {/*<div className="qt-plus" type="button" onClick={this.onItemIncrease}/>*/}
                                        { (item.discount > 0) ?
                                            <h2 className="item-shelf-price" style={{lineHeight: '20px'}}>
                                            <span className="discount-price" >
                                                   ${item.price-(item.price*item.discount/100)} <i>(-{item.discount}%)</i><br/>
                                                   <span className="original-price"> ${item.price} </span>
                                               </span>
                                            </h2> : <h2 className="item-shelf-price" style={{lineHeight: '49px'}}><span>
                                                   {item.price}$
                                               </span>
                                            </h2>
                                        }
                                        <h2 className="price">
                                            <b>Discount:</b> {item.discount}%
                                        </h2>
                                    </footer>
                                </article>

                            )
                        })}
                    {this.state.alert}
                </section>
                {this.props.itemsCount!==null ? <Pagination onGetItems={this.getItems} itemsCount={this.props.itemsCount/4}/> : null}
            </div>
        )
    }
}

export default withRouter(CardShelf);