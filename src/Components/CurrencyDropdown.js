import React, { Component } from 'react';
import './CurrencyDropdown.css'

class CurrencyDropdown extends Component {
    constructor(props) {
        super(props);

        this.state = {
            currencies: [
                {
                    id: 1,
                    name: '$',
                    description: 'USD'
                },
                {
                    id: 2,
                    name: '€',
                    description: 'EUR'
                },
                {
                    id: 3,
                    name: '£',
                    description: 'GBP'
                },
                {
                    id: 4,
                    name: 'p.',
                    description: 'BYN'
                }
            ],
            selectedCurrency: '',
        }
    }

    render() {
        return (
            <div>
                <select className="slct-wrap">
                    {this.state.currencies.map((currency) => {
                        if(currency.id===1){
                            return (
                                <option defaultValue={currency.name} key={currency.id} value={currency.id}>{currency.name} {currency.description}</option>
                            )
                        } else {
                            return (
                                <option key={currency.id} value={currency.id}>{currency.name} {currency.description}</option>
                            )
                        }

                    })}
                </select>
            </div>
        )
    }
}

export default CurrencyDropdown;