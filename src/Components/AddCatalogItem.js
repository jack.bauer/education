import React from "react";
import './AddCatalogItem.css';
import Input from "./Input";
import Select from "./Select";
import Checkbox from "./Checkbox";
import TextArea from "./TextArea";
import ImgCropper from "./ImgCropper";
import CurrencyDropdown from "./CurrencyDropdown";
import axios from 'axios';
import SuccessModal from "./SuccessModal";
import ErrorModal from "./ErrorModal";

class AddCatalogItem extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            manufacturer: '',
            name: '',
            description: '',
            price: '',
            selectedParentCategory: '',
            selectedChildCategory: '',
            parentCategoryId: null,
            childCategoryId: null,
            selectedCurrency: '',
            discount: '',
            quantity: '',
            alert: null,
            successAlert: null,
            selectedProductPhoto: null
        };
    }

    getParentCategory = () => {
        let parentCategories = [];

        this.props.categories.map((index) => {
            if(index.parentCategory===null)  {
                parentCategories.push(index)
            }
            // {[...parentCategories, {}]}
        })

        return parentCategories;
    }

    getChildCategory = (id) => {
        let childCategories = [];

        {this.props.categories.map((index) => {
            if(index.parentCategory){
                if(index.parentCategory.id===id){
                    childCategories.push(index)
                }
            }
        })}
        return childCategories;
    }

    onParentCategoryChanged = (text) => {
        let obj = {};

        let keys = Object.keys(this.props.categories);
        keys.map((key) => {
            if(this.props.categories[key].name===text){
                obj.id = this.props.categories[key].id;
            }
        })

        this.setState({
            parentCategoryId: obj.id,
            selectedParentCategory: text,
        })
    }

    onChildCategoryChanged = (text) => {
        let obj = {};

        let keys = Object.keys(this.props.categories);
        keys.map((key) => {
            if(this.props.categories[key].name===text){
                obj.id = this.props.categories[key].id;
            }
        })

        this.setState({
            childCategoryId: obj.id,
            selectedChildCategory: text,
        })

    }

    onNameChanged = (text) => {
        this.setState({
            name: text,
        })
    }

    onManufacturerChanged = (text) => {
        this.setState({
            manufacturer: text,
        })
    }

    onDescriptionChanged = (text) => {
        this.setState({
            description: text,
        })
    }

    onPriceChanged = (text) => {
        this.setState({
            price: text,
        })
    }

    onDiscountChanged = (text) => {
        this.setState({
            discount: text,
        })
    }

    onQuantityChanged = (text) => {
        this.setState({
            quantity: text,
        })
    }

    closeSuccess = () => {
        this.setState({
            alert: null
        });
    }

    closeFailed = () => {
        this.setState({
            alert: null
        });
    }

    resetView = () => {
        this.setState({
            name: '',
            manufacturer: '',
            description: '',
            discount: '',
            count: '',
            price: '',
            selectedParentCategory: '',
            selectedChildCategory: '',
            quantity: ''
        });
    }

    productPhotoUpdate = (val) => {
        this.setState({
            selectedProductPhoto: val
        });
    }

    addProduct = (e) => {
        e.preventDefault(e);

        let productItem = {}

        if(this.state.selectedProductPhoto!==null){
            productItem = {
                supplier: {
                    id: this.props.user.id
                },
                category:{
                    id: this.state.childCategoryId
                },
                manufacturer: this.state.manufacturer,
                name: this.state.name,
                description: this.state.description,
                quantity: parseInt(this.state.quantity, 999),
                price: parseFloat(this.state.price),
                discount: this.state.discount,
            };
        } else {
            productItem = {
                supplier: {
                    id: this.props.user.id
                },
                category:{
                    id: this.state.childCategoryId
                },
                manufacturer: this.state.manufacturer,
                name: this.state.name,
                description: this.state.description,
                quantity: parseInt(this.state.quantity, 999),
                price: parseFloat(this.state.price),
                discount: this.state.discount
            };
        }

        axios.put('http://localhost:8888/api/v1/items/addItem', productItem)
            .then((response) => {
                console.log(response);
                this.setState({
                    alert: (
                        <SuccessModal title="Success" body="Item was added on your shelf"
                                      onConfirm={() => this.closeSuccess()}/>
                    )
                });
            })
            .catch((response) => {
                console.log(response);
                this.setState({
                    alert: (
                        <ErrorModal errorText="Item was not added. Please try again"
                                    onConfirm={() => this.closeFailed()}/>
                    )
                });
            });

    }

    render() {
        return (
            <div id="item-box">
                <div className="add-item">
                    <h1>Add Item to Catalog</h1>
                    <div>
                        <form onSubmit={this.addProduct}>
                            <div className="flex-container">
                                <div className="col-img">
                                    <ImgCropper onImageCropped={this.productPhotoUpdate} classname="avatar-photo"/>
                                </div>
                                <div className="col-main">
                                    <div className="flex-cont-row">
                                        <Select required={true} name="parentCategory" style={{flex: 1, width: '200px'}} items={this.getParentCategory()} onTextChanged={this.onParentCategoryChanged}
                                                field={this.state.selectedParentCategory} placeholder="Parent Category"/>
                                        <Select required={true} name="childCategory" style={{marginLeft: '10px', width: '200px'}} items={this.getChildCategory(this.state.parentCategoryId)}
                                                onTextChanged={this.onChildCategoryChanged} field={this.state.selectedChildCategory} placeholder="Child Category"/>
                                        <Input className="add-item-input" required min="6" max="20" style={{flex: 1, marginLeft: '20px'}} type="text" onTextChanged={this.onNameChanged}
                                               field={this.state.name} placeholder="Name"/>
                                    </div>
                                    <div className="flex-cont-row" style={{alignItems: 'flex-end'}}>
                                        <Input className="add-item-input" required min="6" max="20" style={{flex: 0.4}} type="text" onTextChanged={this.onManufacturerChanged}
                                               field={this.state.manufacturer} placeholder="Manufacturer"/>
                                        <TextArea placeholder="Description" style={{flex: 1, marginLeft: '20px'}} rowSize="2"
                                                  onTextChanged={this.onDescriptionChanged} field={this.state.description} max="64"/>
                                    </div>
                                    <div className="flex-cont-row">
                                        <Input className="add-item-input" required type="text" style={{flex: 0.7, width: '100px'}} restrictType="text" min="2" max="8"
                                               onTextChanged={this.onPriceChanged} field={this.state.price} placeholder="Price"/>
                                        <CurrencyDropdown style={{flex: 0.7, marginRight: '20px', width: '150px'}}/>
                                        <Input className="add-item-input" style={{flex: 0.7, width: '70px'}} max="2" type="text" onTextChanged={this.onDiscountChanged} field={this.state.discount}
                                               placeholder="Discount %" restrictType="text"/>
                                        <Input className="add-item-input" required style={{flex: 0.7, width: '70px', marginLeft: '20px'}} max="4" type="text" onTextChanged={this.onQuantityChanged}
                                               field={this.state.quantity} placeholder="Quantity" restrictType="text"/>
                                        <div style={{flex: 1, justifyContent: 'center', alignItems: 'center'}} className="reset-product" type="button" onClick={this.resetView}>
                                            <div>Reset</div>
                                            <div className="reset-product-icon"></div>
                                        </div>
                                        {/*<input style={styles.inputButton} className="reset-product" type="button" value="Reset" />*/}
                                        <button style={{flex: 1, justifyContent: 'center', alignItems: 'center'}} className="add-products" type="submit" value="Add Product">
                                            <div>Save</div>
                                            <div className="add-products-icon"></div>
                                        </button>
                                        {/*<input style={{flex: 1}}  type="submit" value="Add product" />*/}
                                    </div>
                                </div>
                            </div>
                        </form>
                        {this.state.alert}
                    </div>
                </div>
            </div>

        )
    }
}

export default AddCatalogItem;