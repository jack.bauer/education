import React from "react";
import './NavigationPanel.css';
import NavLink from "react-router-dom/es/NavLink";
import Authorized from '../Authorization/Authorized';
import { connect } from 'react-redux';
import { logout } from '../Actions/users';
import { cartItemFetch } from '../Actions/items';

class NavigationPanel extends React.Component {
    componentWillMount(){
        this.props.getCartItems()
    }

    render() {
        return (
            <div className="wrapper">
                <nav className="navbar navbar-inverse">
                    <div className="container-fluid">
                        <ul className="nav navbar-nav">
                            <Authorized roles={['Administrator', 'Seller', 'Buyer', 'Premium_Buyer']}>
                                <li><NavLink to="/dashboard" activeClassName="active" title="Dashboard">Dashboard</NavLink></li>
                            </Authorized>
                            <Authorized roles={['Buyer', 'Premium_Buyer']}>
                                <li><NavLink to="/search"title="Search">Search</NavLink></li>
                            </Authorized>
                            <Authorized roles={['Seller']}>
                                <li><NavLink to="/goods" activeClassName="active" title="Goods">Goods</NavLink></li>
                            </Authorized>
                            <Authorized roles={['Administrator', 'Seller', 'Buyer', 'Premium_Buyer']}>
                                <li><NavLink to="/help" title="Help">Help</NavLink></li>
                            </Authorized>
                            <Authorized roles={['Administrator']}>
                                <li><NavLink to="/users" title="Users">Users</NavLink></li>
                            </Authorized>
                        </ul>
                        <ul className="nav navbar-nav navbar-right">
                            <Authorized roles={['Buyer', 'Premium_Buyer']}>
                                <li><NavLink to="/cart" title="Cart">Cart <span className="item-number">{this.props.cartItems.length}</span></NavLink></li>
                            </Authorized>
                            <Authorized roles={['Buyer', 'Premium_Buyer']}>
                                <li><NavLink to="/orders" title="Orders">Orders</NavLink></li>
                            </Authorized>
                            <li className="dropdown">
                                <a className="dropdown-toggle" data-toggle="dropdown" href="#">User
                                    <span className="caret"></span></a>
                                <ul className="dropdown-menu">
                                    <li><NavLink to="/profile" title="Profile"><span className="glyphicon glyphicon-user"></span> Edit Profile</NavLink></li>
                                    <li style={{cursor: 'pointer'}}><a onClick={(e) => {e.preventDefault(); this.props.logout(this.props.userSession.id) }}><span className="log-out-icon"></span> Logout</a></li>
                                </ul>
                            </li>
                        </ul>
                    </div>
                </nav>
            </div>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        cartItems: state.cartItems,
        userSession: state.userSession,
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        logout: (id) => dispatch(logout(id)),
        getCartItems: () => dispatch(cartItemFetch()),
    };
};


export default connect(mapStateToProps, mapDispatchToProps) (NavigationPanel);