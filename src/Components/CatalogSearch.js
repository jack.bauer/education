import React from "react";
import './CatalogSearch.css';
import Input from "./Input";
import Select from "./Select";
import Checkbox from "./Checkbox";
import {withRouter} from 'react-router-dom';
import WarningMessage from './WarningMessage'
// import qs from 'query-string';

class CatalogSearch extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            manufacturer: '',
            name: '',
            description: '',
            minPrice: '',
            maxPrice: '',
            selectedParentCategory: '',
            selectedChildCategory: '',
            categoryId: null,
            childCategoryId: null,
            discount: null,
            inStore: null,
            successAlert: null,
            searchAlert: false
        };
    }

    onManufacturerChanged = (text) => {
        this.setState({
            manufacturer: text,
        })
    }

    onNameChanged = (text) => {
        this.setState({
            name: text,
        })
    }

    onDescriptionChanged = (text) => {
        this.setState({
            description: text,
        })
    }

    onMinPriceChanged = (text) => {
        this.setState({
            minPrice: text,
        })
    }

    onMaxPriceChanged = (text) => {
        this.setState({
            maxPrice: text,
        })
    }

    getParentCategory = () => {
        let parentCategories = [];

        this.props.categories.map((index) => {
            if(index.parentCategory===null)  {
                parentCategories.push(index)
            }
            // {[...parentCategories, {}]}
        })

        return parentCategories;
    }

    getChildCategory = (id) => {
        let childCategories = [];

        {this.props.categories.map((index) => {
            if(index.parentCategory){
                if(index.parentCategory.id===id){
                    childCategories.push(index)
                }
            }
        })}
        return childCategories;
    }

    // onDiscountChanged = (checked) => {
    //     if(checked) {
    //         this.setState({
    //             discount: true,
    //         })
    //         console.log(checked)
    //     } else {

    // }    //         this.setState({
    //             discount: null,
    //         })
    //         console.log(checked)
    //     }
    //

    onParentCategoryChanged = (text) => {
        let obj = {};

        let keys = Object.keys(this.props.categories);
        keys.map((key) => {
            if(this.props.categories[key].name===text){
                obj.id = this.props.categories[key].id;
            }
        })

        this.setState({
            categoryId: obj.id,
            selectedParentCategory: text,
        })
    }

    onChildCategoryChanged = (text) => {
        let obj = {};

        let keys = Object.keys(this.props.categories);
        keys.map((key) => {
            if(this.props.categories[key].name===text){
                obj.id = this.props.categories[key].id;
            }
        })

        this.setState({
            childCategoryId: obj.id,
            selectedChildCategory: text,
        })

    }

    onDiscountChanged = (val) => {
        this.setState({
            discount: val,
        })
    }

    onAvailabilityChanged = (val) => {
        this.setState({
            inStore: val,
        })
    }

    search = () => {
        let itemFilter = {};
        let filter = {itemFilter: {}};
        let emptyFlag = true;

        let keys = Object.keys(this.state).filter((key) => {
            if(key!=='selectedCategory' && key!=='successAlert')
                return key;
        });
        keys.map((key) => {
            if(this.state[key]==="" || this.state[key]===" " || this.state[key]===undefined || this.state[key]===false)
            {
                itemFilter[key] = null;
            } else {
                itemFilter[key] = this.state[key];
            }
        })

        let elements = Object.keys(itemFilter);
        elements.map((el) => {
            if(itemFilter[el]!==null){
                emptyFlag=false;
            }
        })


        if(emptyFlag) {
            if(this.props.onEmptySearch) {
                this.props.onEmptySearch(true);
            }

            throw {
                message: "Empty search",
                code: 403
        }
        } else {
            // let pageNum = qs.parse(this.props.location.search);
            filter.itemFilter = itemFilter;
            filter.page = 0;
            filter.size = 4;


            // console.log(filter);

            this.props.onItemsSearch('http://localhost:8888/api/v1/items/getItemsForPageByFilter', filter);
            if (this.props.onSearchChanged) {
                this.props.onSearchChanged(true);
            }

            if(this.props.filterPass){
                this.props.filterPass(itemFilter);
            }

            if(this.props.onGetCount) {
                this.props.onGetCount(filter);
            }
            this.props.history.push(this.props.location.pathname + '?page=' + 0)
        }

    }

    onReset = () => {
        this.setState({
            manufacturer: '',
            name: '',
            description: '',
            minPrice: '',
            maxPrice: '',
            selectedParentCategory: '',
            selectedChildCategory: '',
            categoryId: null,
            childCategoryId: null,
        });
    }

    render() {
        return (
            <div id="filter-box">
                <div className="filter">
                    <h1>Search</h1>

                    <form >
                        <Select name="parent" items={this.getParentCategory()} onTextChanged={this.onParentCategoryChanged}
                                field={this.state.selectedParentCategory} placeholder="Parent Category"/>
                        <Select name="child" items={this.getChildCategory(this.state.categoryId)} onTextChanged={this.onChildCategoryChanged}
                                field={this.state.selectedChildCategory} placeholder="Sub Category"/>
                        <Input className="cat-search-input" type="text" onTextChanged={this.onManufacturerChanged} field={this.state.manufacturer}
                               placeholder="Manufacturer" />
                        <Input className="cat-search-input" type="text" onTextChanged={this.onNameChanged} field={this.state.name}
                               placeholder="Name" />
                        <Input className="cat-search-input" type="text" onTextChanged={this.onDescriptionChanged} field={this.state.description}
                               placeholder="Description" />
                        <Input className="cat-search-input" type="text" onTextChanged={this.onMinPriceChanged} field={this.state.minPrice}
                               placeholder="Min Price" />
                        <Input className="cat-search-input" type="text" onTextChanged={this.onMaxPriceChanged} field={this.state.maxPrice}
                               placeholder="Max Price" />
                        <div style={{float:'left'}} className="row custom-row">
                            <div className="col-sm-6">
                                <Checkbox label="Discount" onCheckChanged={this.onDiscountChanged}/>
                            </div>
                            <div className="col-sm-6">
                                <Checkbox label="In Store" onCheckChanged={this.onAvailabilityChanged}/>
                            </div>
                        </div>

                        <div className="row custom-row-button">
                            <div className="col-sm-6">
                                <div style={{flex: 1, justifyContent: 'center', alignItems: 'center'}} className="reset-filter" type="button" onClick={this.onReset}>
                                    <div>Reset</div>
                                    <div className="reset-product-icon"></div>
                                    {/*<p style={{float: 'left', marginLeft: '10px', marginTop: '5px'}}>Reset</p> <div className="reset-filter-icon" />*/}
                                </div>
                            </div>
                            <div className="col-sm-6">
                                <div style={{flex: 1, justifyContent: 'center', alignItems: 'center'}} className="get-products" type="button" onClick={this.search}>
                                    <div>Search</div>
                                    <div className="search-filter-icon"></div>
                                    {/*<p style={{float: 'left', marginLeft: '10px', marginTop: '5px'}}>Search</p> <div className="search-filter-icon"/>*/}
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>

        )
    }
}

export default withRouter(CatalogSearch);