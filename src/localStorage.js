const KEY = "SHOPPING_CART";
const SESSION_KEY = "SESSION_KEY";

export const loadState = () => {
    try {
        const serializedState = localStorage.getItem(KEY);

        if (serializedState === null) {
            return [];
        }

        return JSON.parse(serializedState);

    } catch (err) {
        return undefined;
    }
};

export const loadSessionState = () => {
    try {
        const serializedState = localStorage.getItem(SESSION_KEY);

        if (serializedState === null) {
            return {};
        }

        return JSON.parse(serializedState);

    } catch (err) {
        return undefined;
    }
};

export const saveState = (itemState) => {
    try {
        let items = JSON.parse(localStorage.getItem(KEY));
        items = items ? items : [];
        const serializedState = JSON.stringify([...items, itemState]);
        localStorage.setItem(KEY, serializedState);
    } catch (err) {
        console.log(err)
    }
};

export const saveSessionState = (userSession) => {
    try {
        let session = userSession ? userSession : [];
        const serializedState = JSON.stringify(userSession);
        localStorage.setItem(SESSION_KEY, serializedState);
    } catch (err) {
        console.error(err)
    }
};

export const deleteItem = (id) => {
    try {
        let items = JSON.parse(localStorage.getItem(KEY));
        if (items) {
            let newState = items.filter((item) => item.id !== id);
            const serializedState = JSON.stringify(newState);
            localStorage.setItem(KEY, serializedState);
        }
    } catch (err) {
        console.log(err)
    }
};

export const clearStorage = () => {
    try {
        localStorage.removeItem(KEY);
        return [];
    } catch (err) {
        console.error(err)
    }
};

export const clearSession = () => {
    try {
        localStorage.removeItem(SESSION_KEY);
        return {};
    } catch (err) {
        console.error(err)
    }
};

// export const removeFromState = () => {
//     try {
//         const serializedState = localStorage.getItem(KEY);
//
//         if (serializedState === null) {
//             return [];
//         }
//
//         return JSON.parse(serializedState);
//
//     } catch (err) {
//         return undefined;
//     }
// };