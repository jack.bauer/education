import React from "react";
import { Route, Switch } from "react-router-dom";
import Dashboard from "./Containers/Dashboard";
import SignIn from './Containers/LoginContainer';
import UsersList from './Containers/UsersList';
import UserProfile from "./Containers/UserProfile";
import Registration from "./Containers/Registration";
import ProductSearch from "./Containers/ProductSearch";
import ShelfContainer from "./Containers/ShelfContainer";
import Help from "./Components/Help";
import ShoppingCartContainer from "./Containers/ShoppingCartContainer";
import OrdersContainer from "./Containers/OrdersContainer";
import AccessDenied from "./Components/AccessDenied";


export default () =>
    <Switch>
        <Route exact path='/' component={SignIn}/>
        <Route path="/signUp" component={Registration} />
        <Route path="/dashboard" component={Dashboard} roles={['Buyer', 'Seller', 'Administrator', 'Premium_Buyer']}/>
        <Route path="/search" component={ProductSearch} roles={['Buyer', 'Premium_Buyer']}/>
        <Route path="/users" component={UsersList} roles={['Administrator']}/>
        <Route path="/profile" component={UserProfile} roles={['Buyer', 'Seller', 'Administrator', 'Premium_Buyer']}/>
        <Route path="/cart" component={ShoppingCartContainer} roles={['Buyer', 'Premium_Buyer']}/>
        <Route path="/orders" component={OrdersContainer} roles={['Buyer', 'Premium_Buyer']}/>
        <Route path="/goods" component={ShelfContainer} roles={['Seller']}/>
        <Route path="/help" component={Help} roles={['Buyer', 'Seller', 'Administrator', 'Premium_Buyer']}/>
        <Route path="/restricted" component={AccessDenied} />
    </Switch>;