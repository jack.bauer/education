import * as types from './actionTypes';

export const roleHasErrored = (bool) => {
    return {
        type: types.ROLE_HAS_ERRORED,
        hasErrored: bool
    }
};

export const roleIsLoading = (bool) => {
    return {
        type: types.ROLE_IS_LOADING,
        isLoading: bool
    }
};

export const roleFetchDataSuccess = (roles) => {
    return {
        type: types.ROLE_FETCH_DATA_SUCCESS,
        roles
    }
};

export const rolesFetchData = (url) => {
    return (dispatch) => {
        dispatch(roleIsLoading(true));

        fetch(url)
            .then((response) => {
                if (!response.ok) {
                    throw Error(response.statusText);
                }

                dispatch(roleIsLoading(false));

                return response;
            })
            .then((response) => response.json())
            .then((roles) => dispatch(roleFetchDataSuccess(roles)))
            .catch(() => dispatch(roleHasErrored(true)));
    }
};