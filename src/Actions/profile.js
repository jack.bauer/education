import * as types from './actionTypes';

export const updateUserProfileSuccess = (profile) => {
    return {
        type: types.UPDATE_PROFILE_SUCCESS,
        payload: profile
    }
};

export const profileHasErrored = (bool) => {
    return {
        type: types.PROFILE_HAS_ERRORED,
        profileHasErrored: bool
    }
};

export const profileIsLoading = (bool) => {
    return {
        type: types.PROFILE_IS_LOADING,
        profileIsLoading: bool
    }
};

export const profileFetchDataSuccess = (profile) => {
    return {
        type: types.PROFILE_FETCH_DATA_SUCCESS,
        profile
    }
};


export const profileFetchData = (id) => {
    return (dispatch) => {
        dispatch(profileIsLoading(true));

        fetch('http://localhost:8888/api/v1/users/getUserInfo/'+ id)
            .then((response) => {
                if (!response.ok && response.status!==302) {
                    throw Error(response.statusText);
                }

                dispatch(profileIsLoading(false));

                return response;
            })
            .then((response) => response.json())
            .then((profile) => dispatch(profileFetchDataSuccess(profile)))
            .catch(() => dispatch(profileHasErrored(true)));
    }
};