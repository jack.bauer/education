import * as types from './actionTypes';

export const categoryHasErrored = (bool) => {
    return {
        type: types.CATEGORY_HAS_ERRORED,
        categoryHasErrored: bool
    }
};

export const categoryIsLoading = (bool) => {
    return {
        type: types.CATEGORY_IS_LOADING,
        categoryIsLoading: bool
    }
};

export const categoryFetchDataSuccess = (categories) => {
    return {
        type: types.CATEGORY_FETCH_DATA_SUCCESS,
        categories
    }
};

export const categoryFetchData = (url) => {
    return (dispatch) => {
        dispatch(categoryIsLoading(true));

        fetch(url)
            .then((response) => {
                if (!response.ok) {
                    throw Error(response.statusText);
                }

                dispatch(categoryIsLoading(false));

                return response;
            })
            .then((response) => response.json())
            .then((categories) => dispatch(categoryFetchDataSuccess(categories)))
            .catch(() => dispatch(categoryHasErrored(true)));
    }
};