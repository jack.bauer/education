import * as types from './actionTypes';
import axios from 'axios';
import { saveSessionState, loadSessionState, clearSession } from "../localStorage"
import { push } from 'react-router-redux'

export const addUserSuccess = (user) => {
    return {
        type: types.ADD_USER_SUCCESS,
        payload: user
    }
};

export const userHasErrored = (bool) => {
    return {
        type: types.USER_HAS_ERRORED,
        hasErrored: bool
    }
};

export const userIsLoading = (bool) => {
    return {
        type: types.USER_IS_LOADING,
        isLoading: bool
    }
};

export const userFetchDataSuccess = (users) => {
    return {
        type: types.USER_FETCH_DATA_SUCCESS,
        users
    }
};

export const usersFetchData = (url) => {
    return (dispatch) => {
        dispatch(userIsLoading(true));

        fetch(url)
            .then((response) => {
                if (!response.ok ) {
                    console.log(response)
                    throw Error(response.statusText);
                }

                dispatch(userIsLoading(false));

                return response;
            })
            .then((response) => response.json())
            .then((users) => dispatch(userFetchDataSuccess(users)))
            .catch(() => dispatch(userHasErrored(true)));
    }
};

export const login = (obj) => {
    return (dispatch) => {
        dispatch(loginInProgress(true));

        axios.post('http://localhost:8888/api/v1/users/login', obj)
            .then((response) => response.data)
            .then((userSession) => dispatch(addSessionInfo(userSession)))
            .then(() => dispatch(loginSuccess()))
            .then(() => dispatch(push('dashboard')))
            // .then(() => dispatch(purchaseInProgress(false)))
            .catch((e) => {console.error(e);dispatch(loginHasErrored(true))});
    }
};

export const loginHasErrored = (bool) => {
    return {
        type: types.LOGIN_HAS_ERRORED,
        loginHasErrored: bool
    }
};

export const loginInProgress = (bool) => {
    return {
        type: types.LOGIN_IN_PROGRESS,
        loginInProgress: bool
    }
};

export const loginSuccess = () => {
    return {
        type: types.LOGIN_SUCCESS,
        loginSuccess: true
    }
};

//Logout
export const logout = (obj) => {
    return (dispatch) => {
        dispatch(logoutInProgress(true));

        axios.get('http://localhost:8888/api/v1/users/logout?id=', obj)
            .then((response) => response.data)
            .then(() => dispatch(clearSessionInfo()))
            .then(() => dispatch(logoutSuccess()))
            .then(() => dispatch(push('/')))
            .catch((e) => {console.error(e);dispatch(logoutHasErrored(true))});
    }
};

export const logoutHasErrored = (bool) => {
    return {
        type: types.LOGOUT_HAS_ERRORED,
        logoutHasErrored: bool
    }
};

export const logoutInProgress = (bool) => {
    return {
        type: types.LOGOUT_IN_PROGRESS,
        logoutInProgress: bool
    }
};

export const logoutSuccess = () => {
    return {
        type: types.LOGOUT_SUCCESS,
        logoutSuccess: true
    }
};

export const clearSessionInfo = () => {
    let userSession = clearSession();
    return {
        type: types.LOGOUT,
        userSession
    }
};

////////////////////////////////////////////////////////////////////////////////////////////////

export const addSessionInfo = (userSession) => {
    saveSessionState(userSession);
    return {
        type: types.LOGIN,
        userSession
    }
};

export const userSessionFetch = () => {
    let userSession = loadSessionState();
    return {
        type: types.FETCH_USER_SESSION,
        userSession
    }
};

export const fetchUsersCount = () => {
    return (dispatch) => {
        dispatch(usersCountFetchInProgress(true));

        axios.get('http://localhost:8888/api/v1/users/getCountNotAdmin')
            .then((response) => response.data)
            .then((usersCount) => dispatch(usersCountFetch(usersCount)))
            .then(() => dispatch(usersCountFetchSuccess()))
            // .then(() => dispatch(usersCountFetchInProgress(false)))
            .catch((e) => {console.error(e);dispatch(usersCountFetchHasErrored(true))});
    }
};

export const usersCountFetch = (usersCount) => {
    return {
        type: types.USERS_COUNT_FETCH,
        usersCount
    }
};

export const usersCountFetchHasErrored = (bool) => {
    return {
        type: types.USERS_COUNT_FETCH_HAS_ERRORED,
        usersFetchHasErrored: bool
    }
};

export const usersCountFetchInProgress = (bool) => {
    return {
        type: types.USERS_COUNT_FETCH_IN_PROGRESS,
        usersFetchInProgress: bool
    }
};

export const usersCountFetchSuccess = (bool) => {
    return {
        type: types.USERS_COUNT_FETCH_SUCCESS,
        usersFetchSuccess: bool
    }
};

//Disabled users
export const fetchDisabledUsersCount = () => {
    return (dispatch) => {
        dispatch(disabledUsersCountFetchInProgress(true));

        axios.get('http://localhost:8888/api/v1/users/getCountDisabledNotAdmin')
            .then((response) => response.data)
            .then((disabledUsersCount) => dispatch(disabledUsersCountFetch(disabledUsersCount)))
            .then(() => dispatch(disabledUsersCountFetchSuccess()))
            .catch((e) => {console.error(e);dispatch(disabledUsersCountFetchHasErrored(true))});
    }
};

export const disabledUsersCountFetch = (disabledUsersCount) => {
    return {
        type: types.DISABLED_USERS_COUNT_FETCH,
        disabledUsersCount
    }
};

export const disabledUsersCountFetchHasErrored = (bool) => {
    return {
        type: types.DISABLED_USERS_COUNT_FETCH_HAS_ERRORED,
        disabledUsersFetchHasErrored: bool
    }
};

export const disabledUsersCountFetchInProgress = (bool) => {
    return {
        type: types.DISABLED_USERS_COUNT_FETCH_IN_PROGRESS,
        disabledUsersFetchInProgress: bool
    }
};

export const disabledUsersCountFetchSuccess = (bool) => {
    return {
        type: types.DISABLED_USERS_COUNT_FETCH_SUCCESS,
        disabledUsersFetchSuccess: bool
    }
};

//Users without items
export const fetchCountOfUsersWithoutItems = () => {
    return (dispatch) => {
        dispatch(usersWithoutItemsFetchInProgress(true));

        axios.get('http://localhost:8888/api/v1/users/getCountNotAdminWithoutItems')
            .then((response) => response.data)
            .then((usersWithoutItemsCount) => dispatch(usersWithoutItemsFetch(usersWithoutItemsCount)))
            .then(() => dispatch(usersWithoutItemsFetchSuccess()))
            .catch((e) => {console.error(e);dispatch(usersWithoutItemsFetchHasErrored(true))});
    }
};

export const usersWithoutItemsFetch = (usersWithoutItemsCount) => {
    return {
        type: types.USERS_WITHOUT_ITEMS_COUNT_FETCH,
        usersWithoutItemsCount
    }
};

export const usersWithoutItemsFetchHasErrored = (bool) => {
    return {
        type: types.USERS_WITHOUT_ITEMS_COUNT_FETCH_HAS_ERRORED,
        usersWithoutItemsFetchHasErrored: bool
    }
};

export const usersWithoutItemsFetchInProgress = (bool) => {
    return {
        type: types.USERS_WITHOUT_ITEMS_COUNT_FETCH_IN_PROGRESS,
        usersWithoutItemsFetchInProgress: bool
    }
};

export const usersWithoutItemsFetchSuccess = (bool) => {
    return {
        type: types.USERS_WITHOUT_ITEMS_COUNT_FETCH_SUCCESS,
        usersWithoutItemsFetchSuccess: bool
    }
};

