import * as types from './actionTypes';
import axios from 'axios';
import { v4 } from 'node-uuid';
import { loadState, saveState, clearStorage, deleteItem } from "../localStorage"

export const filterdItemHasErrored = (bool) => {
    return {
        type: types.FILTERED_ITEM_HAS_ERRORED,
        filteredItemHasErrored: bool
    }
};

export const filteredItemIsLoading = (bool) => {
    return {
        type: types.FILTERED_ITEM_IS_LOADING,
        filteredItemIsLoading: bool
    }
};

export const filteredItemFetchSuccess = (filteredItems) => {
    return {
        type: types.FILTERED_ITEM_FETCH_SUCCESS,
        filteredItems
    }
};

export const filteredItemFetch = (url, obj) => {
    return (dispatch) => {
        dispatch(filteredItemIsLoading(true));

        axios.post(url, obj)
            .then((response) => response.data)
            .then((filteredItems) => dispatch(filteredItemFetchSuccess(filteredItems)))
            .then(() => dispatch(filteredItemIsLoading(false)))
            .catch(() => dispatch(filterdItemHasErrored(true)));
    }
};

export const itemHasErrored = (bool) => {
    return {
        type: types.ITEM_HAS_ERRORED,
        itemHasErrored: bool
    }
};

export const itemIsLoading = (bool) => {
    return {
        type: types.ITEM_IS_LOADING,
        itemIsLoading: bool
    }
};

export const itemFetchSuccess = (items) => {
    return {
        type: types.ITEM_FETCH_SUCCESS,
        items
    }
};

export const itemsFetchData = (url) => {
    return (dispatch) => {
        dispatch(itemIsLoading(true));

        fetch(url)
            .then((response) => response.json())
            .then((items) => dispatch(itemFetchSuccess(items)))
            .catch(() => dispatch(itemHasErrored(true)));
    }
};

//items count for seller
export const itemCountHasErrored = (bool) => {
    return {
        type: types.ITEM_COUNT_HAS_ERRORED,
        itemCountHasErrored: bool
    }
};

export const itemCountIsLoading = (bool) => {
    return {
        type: types.ITEM_COUNT_IS_LOADING,
        itemCountIsLoading: bool
    }
};

export const itemCountFetchSuccess = (itemsCount) => {
    return {
        type: types.ITEM_COUNT_FETCH_SUCCESS,
        itemsCount
    }
};

export const itemsCountFetchData = (url) => {
    return (dispatch) => {
        dispatch(itemCountIsLoading(true));

        axios.get(url)
            .then((response) => response.data)
            .then((itemsCount) => dispatch(itemCountFetchSuccess(itemsCount)))
            .then(() => dispatch(itemCountIsLoading(false)))
            .catch(() => dispatch(itemCountHasErrored(true)));
    }
};

//items count for buyer
export const filteredItemCountHasErrored = (bool) => {
    return {
        type: types.FILTERED_ITEM_COUNT_HAS_ERRORED,
        filteredItemCountHasErrored: bool
    }
};

export const filteredItemCountIsLoading = (bool) => {
    return {
        type: types.FILTERED_ITEM_COUNT_IS_LOADING,
        filteredItemCountIsLoading: bool
    }
};

export const filteredItemCountFetchSuccess = (filteredItemsCount) => {
    return {
        type: types.FILTERED_ITEM_COUNT_FETCH_SUCCESS,
        filteredItemsCount
    }
};

export const filteredItemsCountFetchData = (url, obj) => {
    return (dispatch) => {
        dispatch(itemCountIsLoading(true));

        axios.post(url, obj)
            .then((response) => response.data)
            .then((filteredItemsCount) => dispatch(filteredItemCountFetchSuccess(filteredItemsCount)))
            .then(() => dispatch(filteredItemCountIsLoading(false)))
            .catch(() => dispatch(filteredItemCountHasErrored(true)));
    }
};

export const cartItemFetch = () => {
    let cartItems = loadState();
    return {
        type: types.GET_ALL_CART_ITEMS,
        cartItems
    }
};

export const addCartItem = (cartItem) => {
    saveState(cartItem);
    return {
        type: types.ADD_CART_ITEM,
        cartItem
    }
};

export const deleteCartItem = (id) => {
    deleteItem(id);
    return {
        type: types.DELETE_CART_ITEM,
        id
    }
};

export const clearCartItems = () => {
    let cartItems = clearStorage();
    return {
        type: types.CLEAR_CART,
        cartItems
    }
}

export const purchaseItems = (obj) => {
    return (dispatch) => {
        dispatch(purchaseInProgress(true));

        axios.put('http://localhost:8888/api/v1/orders/addOrder', obj)
            .then((response) => response.data)
            .then(() => dispatch(purchaseSuccess()))
            .then(() => dispatch(clearCartItems()))
            // .then(() => dispatch(purchaseInProgress(false)))
            .catch((e) => {console.error(e);dispatch(purchaseHasErrored(true))});
    }
};

export const purchaseHasErrored = (bool) => {
    return {
        type: types.PURCHASE_HAS_ERRORED,
        purchaseHasErrored: bool
    }
};

export const purchaseInProgress = (bool) => {
    return {
        type: types.PURCHASE_IN_PROGRESS,
        purchaseInProgress: bool
    }
};

export const purchaseSuccess = () => {
    return {
        type: types.PURCHASE_SUCCESS,
        purchaseSuccess: true
    }
};


//Get all items
export const fetchAllItemsCount = () => {
    return (dispatch) => {
        dispatch(itemsCountFetchInProgress(true));

        axios.get('http://localhost:8888/api/v1/items/getCountOfAllItems')
            .then((response) => response.data)
            .then((itemsCount) => dispatch(allItemsCountFetch(itemsCount)))
            .then(() => dispatch(itemsCountFetchSuccess()))
            .catch((e) => {console.error(e);dispatch(itemsCountFetchHasErrored(true))});
    }
};

export const allItemsCountFetch = (allItemsCount) => {
    return {
        type: types.ALL_ITEMS_COUNT_FETCH,
        allItemsCount
    }
};

export const itemsCountFetchHasErrored = (bool) => {
    return {
        type: types.ALL_ITEMS_COUNT_FETCH_HAS_ERRORED,
        allItemsFetchHasErrored: bool
    }
};

export const itemsCountFetchInProgress = (bool) => {
    return {
        type: types.ALL_ITEMS_COUNT_FETCH_IN_PROGRESS,
        allItemsFetchInProgress: bool
    }
};

export const itemsCountFetchSuccess = (bool) => {
    return {
        type: types.ALL_ITEMS_COUNT_FETCH_SUCCESS,
        allItemsFetchSuccess: bool
    }
};

//Get all items with discount
export const fetchCountWithDiscount = () => {
    return (dispatch) => {
        dispatch(fetchCountOfAllItemsWithDiscountInProgress(true));

        axios.get('http://localhost:8888/api/v1/items/getAllItemsWithDiscount')
            .then((response) => response.data)
            .then((countOfAllItemsWithDiscount) => dispatch(fetchCountOfAllItemsWithDiscount(countOfAllItemsWithDiscount)))
            .then(() => dispatch(fetchCountOfItemsWithDiscountSuccess()))
            .catch((e) => {console.error(e);dispatch(fetchCountOfAllItemsWithDiscountHasErrored(true))});
    }
};

export const fetchCountOfAllItemsWithDiscount = (countOfAllItemsWithDiscount) => {
    return {
        type: types.FETCH_COUNT_OF_ALL_ITEMS_WITH_DISCOUNT,
        countOfAllItemsWithDiscount
    }
};

export const fetchCountOfAllItemsWithDiscountHasErrored = (bool) => {
    return {
        type: types.FETCH_COUNT_OF_ALL_ITEMS_WITH_DISCOUNT_HAS_ERRORED,
        itemsCountWithDiscountHasErrored: bool
    }
};

export const fetchCountOfAllItemsWithDiscountInProgress = (bool) => {
    return {
        type: types.FETCH_COUNT_OF_ALL_ITEMS_WITH_DISCOUNT_IN_PROGRESS,
        itemsCountWithDiscountInProgress: bool
    }
};

export const fetchCountOfItemsWithDiscountSuccess = (bool) => {
    return {
        type: types.FETCH_COUNT_OF_ALL_ITEMS_WITH_DISCOUNT_SUCCESS,
        itemsCountWithDiscountSuccess: bool
    }
};

//Get all items with zero quantity
export const fetchCountWithZeroQty = (id) => {
    return (dispatch) => {
        dispatch(fetchCountOfAllItemsWithZeroQtyInProgress(true));

        axios.get('http://localhost:8888/api/v1/items/getAllItemsWithZeroQuantity/' + id)
            .then((response) => response.data)
            .then((countOfAllItemsWithZeroQty) => dispatch(fetchCountOfAllItemsWithZeroQty(countOfAllItemsWithZeroQty)))
            .then(() => dispatch(fetchCountOfItemsWithZeroQtySuccess()))
            .catch((e) => {console.error(e);dispatch(fetchCountOfAllItemsWithZeroQtyHasErrored(true))});
    }
};

export const fetchCountOfAllItemsWithZeroQty = (countOfAllItemsWithZeroQty) => {
    return {
        type: types.FETCH_COUNT_OF_ITEMS_WITH_ZERO_QUANTITY,
        countOfAllItemsWithZeroQty
    }
};

export const fetchCountOfAllItemsWithZeroQtyHasErrored = (bool) => {
    return {
        type: types.FETCH_COUNT_OF_ITEMS_WITH_ZERO_QUANTITY_HAS_ERRORED,
        itemsCountWithZeroQtyHasErrored: bool
    }
};

export const fetchCountOfAllItemsWithZeroQtyInProgress = (bool) => {
    return {
        type: types.FETCH_COUNT_OF_ITEMS_WITH_ZERO_QUANTITY_IN_PROGRESS,
        itemsCountWithZeroQtyInProgress: bool
    }
};

export const fetchCountOfItemsWithZeroQtySuccess = (bool) => {
    return {
        type: types.FETCH_COUNT_OF_ITEMS_WITH_ZERO_QUANTITY_SUCCESS,
        itemsCountWithZeroQtySuccess: bool
    }
};

//Get count of all disabled items
export const fetchCountOfDisabledItemsById = (id) => {
    return (dispatch) => {
        dispatch(fetchCountOfAllDisabledItemsInProgress(true));

        axios.get('http://localhost:8888/api/v1/items/getCountOfAllDisabledItems/' + id)
            .then((response) => response.data)
            .then((countOfDisabledItems) => dispatch(fetchCountOfAllDisabledItems(countOfDisabledItems)))
            .then(() => dispatch(fetchCountOfAllDisabledItemsSuccess()))
            .catch((e) => {console.error(e);dispatch(fetchCountOfAllDisabledItemsHasErrored(true))});
    }
};

export const fetchCountOfAllDisabledItems = (countOfDisabledItems) => {
    return {
        type: types.FETCH_COUNT_OF_DISABLED_ITEMS,
        countOfDisabledItems
    }
};

export const fetchCountOfAllDisabledItemsHasErrored = (bool) => {
    return {
        type: types.FETCH_COUNT_OF_DISABLED_ITEMS_HAS_ERRORED,
        disabledItemsCountHasErrored: bool
    }
};

export const fetchCountOfAllDisabledItemsInProgress = (bool) => {
    return {
        type: types.FETCH_COUNT_OF_DISABLED_ITEMS_IN_PROGRESS,
        disabledItemsCountInProgress: bool
    }
};

export const fetchCountOfAllDisabledItemsSuccess = (bool) => {
    return {
        type: types.FETCH_COUNT_OF_DISABLED_ITEMS_SUCCESS,
        disabledItemsCountSuccess: bool
    }
};