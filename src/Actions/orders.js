import * as types from './actionTypes';
import axios from 'axios';

export const orderHasErrored = (bool) => {
    return {
        type: types.ORDER_HAS_ERRORED,
        orderHasErrored: bool
    }
};

export const orderIsLoading = (bool) => {
    return {
        type: types.ORDER_IS_LOADING,
        orderIsLoading: bool
    }
};

export const orderFetchSuccess = (orders) => {
    return {
        type: types.ORDER_FETCH_SUCCESS,
        orders
    }
};

export const getOrderItems = (id) => {
    return (dispatch) => {
        dispatch(orderIsLoading(true));

        axios.get('http://localhost:8888/api/v1/orders/getOrders/' + id)
            .then((response) => response.data)
            .then((orders) => dispatch(orderFetchSuccess(orders)))
            .catch(() => dispatch(orderHasErrored(true)));
    }
};

//Get all orders count
export const fetchOrdersCount = (id) => {
    return (dispatch) => {
        dispatch(allOrdersCountFetchInProgress(true));

        axios.get('http://localhost:8888/api/v1/orders/getOrdersCount/' + id)
            .then((response) => response.data)
            .then((allOrdersCount) => dispatch(allOrdersCountFetch(allOrdersCount)))
            .then(() => dispatch(allOrdersCountFetchSuccess()))
            .catch((e) => {console.error(e);dispatch(allOrdersCountFetchHasErrored(true))});
    }
};

export const allOrdersCountFetch = (allOrdersCount) => {
    return {
        type: types.ORDERS_COUNT_FETCH,
        allOrdersCount
    }
};

export const allOrdersCountFetchHasErrored = (bool) => {
    return {
        type: types.ORDERS_COUNT_FETCH_HAS_ERRORED,
        allOrdersCountFetchHasErrored: bool
    }
};

export const allOrdersCountFetchInProgress = (bool) => {
    return {
        type: types.ORDERS_COUNT_FETCH_IN_PROGRESS,
        allOrdersCountFetchInProgress: bool
    }
};

export const allOrdersCountFetchSuccess = (bool) => {
    return {
        type: types.ORDERS_COUNT_FETCH_SUCCESS,
        allOrdersCountFetchSuccess: bool
    }
};

